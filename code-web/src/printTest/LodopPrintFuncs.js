import { Message } from 'element-ui';
var baseURL = "http://localhost:8000";

//判断打印名称是否存在
export function printName(printNam4) {
    var iCount=LODOP.GET_PRINTER_COUNT();
    var printName =[];
    for(var i= 0 ; i<iCount ;i++){
        printName.push(LODOP.GET_PRINTER_NAME(i))
    }
    console.log(iCount,"iCount-------")
    console.log(printName,"printName-------")
    var printType = false;
    if(printName.indexOf(printNam4)>=0){
        printType = true;
    }
    return printType;
}

// 打印数据表单
export function printProjectData(data ,title) {
  //字体大小标题
  var fontSizeBt = 16;
  //字体大小 字段名
  var fontSizeZdm = 10;
  //字体大小 字段值
  var fontSizeZdz = 8;

  //线段长度 表宽
  var lineSizeBk = 600;

  //列宽
  var lineSizeLk  = 10;

  //初始距离上方高度
  var top  = 10;
  //初始距离左方高度
  var left  = 10;

  // 字与线的间隔
  var jg = 7;
  //线条距上部的高度
  var lineTop = top+fontSizeBt+jg;

  LODOP.SET_PRINT_STYLE("FontName" ,"宋体");
  LODOP.SET_PRINT_STYLE("FontSize" ,fontSizeBt);
  LODOP.ADD_PRINT_TEXT(top,lineSizeBk/2-lineSizeLk,fontSizeBt*8,fontSizeBt,title);
  LODOP.ADD_PRINT_LINE(lineTop, left, lineTop, left+lineSizeBk, 0, 1);



  //第一行 下方高度位置
  var fistLineTop = lineTop+fontSizeZdm+jg*2
  //第一行 值的宽度  （表宽 - 三个字段名的长度） / 3
  var valueWith  =  lineSizeBk / 3;

  LODOP.SET_PRINT_STYLE("FontSize" ,fontSizeZdm);
  LODOP.ADD_PRINT_LINE(lineTop, left ,fistLineTop, left ,0,1  );
  LODOP.ADD_PRINT_TEXT(lineTop+jg , left+jg , fontSizeZdm*6 + fontSizeZdm*16 , fontSizeZdm,"客户："+data.clientName);
  LODOP.ADD_PRINT_LINE(lineTop , left + valueWith, fistLineTop , left + valueWith, 0, 1);
  LODOP.ADD_PRINT_TEXT(lineTop+jg , left + jg + valueWith , fontSizeZdm*6 + fontSizeZdm*16 , fontSizeZdm,"项目："+data.projectName);
  LODOP.ADD_PRINT_LINE(lineTop , left + valueWith*2, fistLineTop , left + valueWith*2, 0, 1);
  LODOP.ADD_PRINT_TEXT(lineTop+jg , left + jg + valueWith*2 , fontSizeZdm*6 + fontSizeZdm*16 , fontSizeZdm,"电话："+data.phone);
  LODOP.ADD_PRINT_LINE(lineTop , left + lineSizeBk , fistLineTop , left + lineSizeBk , 0, 1);

  //第二行 下方高度位置
  var secondLineTop  =  fistLineTop + fontSizeZdm + jg*2;
  LODOP.ADD_PRINT_LINE(fistLineTop , left, fistLineTop , left+lineSizeBk, 0, 1);
  LODOP.ADD_PRINT_LINE(fistLineTop , left, secondLineTop ,left, 0, 1);
  LODOP.ADD_PRINT_TEXT(secondLineTop-fontSizeZdm-jg , left + jg , fontSizeZdm*8 + fontSizeZdm*16 , fontSizeZdm,"加工日期："+data.makeDate);
  // LODOP.ADD_PRINT_LINE(fistLineTop , left+valueWith , secondLineTop ,left+valueWith, 0, 1);
  // LODOP.ADD_PRINT_TEXT(secondLineTop-fontSizeZdm-jg , left+valueWith+jg , fontSizeZdm*8 + fontSizeZdm*16 , fontSizeZdm,"含税："+(data.pluxDutyFlag == 1?"含":"不含") + "     " +"含运费："+(data.freightFlag == 1?"含":"不含"));
  LODOP.ADD_PRINT_LINE(fistLineTop , left+lineSizeBk , secondLineTop ,left+lineSizeBk , 0, 1);

  //第三行 下方高度位置  第二行高度加 一个字的高度 加两个间隔的高度
  var thirdlyLineTop = secondLineTop+fontSizeZdm+jg*2;
  var 单位宽度 = 600/20;
  var 编号 = 单位宽度 * 2;
  var 产品名称 = 单位宽度 * 4;
  var 型号 = 单位宽度 * 2;
  var 内直径 = 单位宽度 * 2;
  var 长 = 单位宽度;
  var 宽 = 单位宽度;
  var 长度 = 单位宽度 * 2;
  var 单位 = 单位宽度 * 2;
  var 数量 = 单位宽度 * 2;
  var 孔数量 = 单位宽度 * 2;
  var 备注 = 单位宽度 * 2;
  //第三行距左方距离
  var thiredLeft =  left ;
  LODOP.ADD_PRINT_LINE(secondLineTop , left , secondLineTop , left+lineSizeBk , 0, 1);

  if(data.projectType==1){
    //第三行 标题 竖线
    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop , thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 编号 , fontSizeZdm,"编号");
    thiredLeft = thiredLeft + 编号;
    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop ,thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 产品名称 , fontSizeZdm,"产品名称");
    thiredLeft = thiredLeft + 产品名称;
    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop , thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 型号 , fontSizeZdm,"型号");
    thiredLeft = thiredLeft + 型号;
    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop , thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 长 , fontSizeZdm,"长");
    thiredLeft = thiredLeft + 长;
    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop , thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 宽 , fontSizeZdm,"宽");
    thiredLeft = thiredLeft + 宽;
    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop , thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 长度 , fontSizeZdm,"长度");
    thiredLeft = thiredLeft + 长度;
    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop , thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 单位 , fontSizeZdm,"单位");
    thiredLeft = thiredLeft + 单位;
    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop , thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 数量 , fontSizeZdm,"数量");
    thiredLeft = thiredLeft + 数量;
    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop , thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 备注 , fontSizeZdm,"备注");
    LODOP.ADD_PRINT_LINE(secondLineTop , left+ lineSizeBk  , thirdlyLineTop , left+ lineSizeBk , 0, 1);
    LODOP.ADD_PRINT_LINE(thirdlyLineTop , left  , thirdlyLineTop , left+ lineSizeBk , 0, 1);

    //第四行行高
    var fourthlyLineTop =  thirdlyLineTop+fontSizeZdm+jg*2;
    var fourthlyLeft =  left ;

    //循环打印列表内容
    if(data.configData){
        var i = 0;
        for(var project of data.configData){
        i = i+1;
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop , fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 编号 , fontSizeZdm, i+"");
        fourthlyLeft = fourthlyLeft + 编号;
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop ,fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 产品名称 , fontSizeZdm, project.productName );
        fourthlyLeft = fourthlyLeft + 产品名称;
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop , fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 型号 , fontSizeZdm, project.model );
        fourthlyLeft = fourthlyLeft + 型号;
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop , fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 长 , fontSizeZdm,project.long1 );
        fourthlyLeft = fourthlyLeft + 长;
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop , fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 宽 , fontSizeZdm, project.wide );
        fourthlyLeft = fourthlyLeft + 宽;
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop , fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 长度 , fontSizeZdm,project.length );
        fourthlyLeft = fourthlyLeft + 长度;
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop , fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 单位 , fontSizeZdm, project.unit );
        fourthlyLeft = fourthlyLeft + 单位;
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop , fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 数量 , fontSizeZdm, project.quantity);
        fourthlyLeft = fourthlyLeft + 数量;
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop , fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 备注 , fontSizeZdm, project.remark );
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , left+ lineSizeBk  , thirdlyLineTop , left+ lineSizeBk , 0, 1);
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , left  , thirdlyLineTop , left+ lineSizeBk , 0, 1);

        //循环晚
        thirdlyLineTop  = thirdlyLineTop+fontSizeZdm+jg*2;
        fourthlyLineTop = thirdlyLineTop+fontSizeZdm+jg*2;
        fourthlyLeft =  left;
        }

        //合计行
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , left  , thirdlyLineTop , left+ lineSizeBk , 0, 1);


        // LODOP.ADD_PRINT_LINE(thirdlyLineTop , left  , fourthlyLineTop , left , 0, 1);
        // LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , left+jg , 备注 , fontSizeZdm, "合计：" );
        // LODOP.ADD_PRINT_LINE(fourthlyLineTop , left  , fourthlyLineTop , left+ lineSizeBk , 0, 1);

        //右边竖线
        LODOP.ADD_PRINT_LINE(lineTop , left+ lineSizeBk  , thirdlyLineTop , left+ lineSizeBk , 0, 1);
    }
  }else {
    //第三行 标题 竖线
    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop , thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 编号 , fontSizeZdm,"编号");
    thiredLeft = thiredLeft + 编号;
    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop ,thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 产品名称 , fontSizeZdm,"产品名称");
    thiredLeft = thiredLeft + 产品名称;

    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop ,thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 产品名称 , fontSizeZdm,"内直径");
    thiredLeft = thiredLeft + 内直径;

    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop , thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 长 , fontSizeZdm,"数量");
    thiredLeft = thiredLeft + 数量;
    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop , thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 宽 , fontSizeZdm,"孔数量");
    thiredLeft = thiredLeft + 孔数量;
    LODOP.ADD_PRINT_LINE(secondLineTop , thiredLeft , thirdlyLineTop , thiredLeft , 0, 1);
    LODOP.ADD_PRINT_TEXT(secondLineTop+jg , thiredLeft+jg , 备注*16, fontSizeZdm,"备注");
    LODOP.ADD_PRINT_LINE(secondLineTop , left+ lineSizeBk  , thirdlyLineTop , left+ lineSizeBk , 0, 1);
    LODOP.ADD_PRINT_LINE(thirdlyLineTop , left  , thirdlyLineTop , left+ lineSizeBk , 0, 1);

    //第四行行高
    var fourthlyLineTop =  thirdlyLineTop+fontSizeZdm+jg*2;
    var fourthlyLeft =  left ;

    //循环打印列表内容
    if(data.configData){
        var i = 0;
        for(var project of data.configData){
        i = i+1;
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop , fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 编号 , fontSizeZdm, i+"");
        fourthlyLeft = fourthlyLeft + 编号;
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop ,fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 产品名称 , fontSizeZdm, project.productName );
        fourthlyLeft = fourthlyLeft + 产品名称;

        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop ,fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 内直径 , fontSizeZdm, project.innerDiameter );
        fourthlyLeft = fourthlyLeft + 内直径;

        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop , fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 数量 , fontSizeZdm,project.quantity );
        fourthlyLeft = fourthlyLeft + 数量;
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop , fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 孔数量 , fontSizeZdm, project.holeCount );
        fourthlyLeft = fourthlyLeft + 孔数量;
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , fourthlyLeft , fourthlyLineTop , fourthlyLeft , 0, 1);
        LODOP.ADD_PRINT_TEXT(thirdlyLineTop+jg , fourthlyLeft+jg , 备注*16 , fontSizeZdm, project.remark );
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , left+ lineSizeBk  , thirdlyLineTop , left+ lineSizeBk , 0, 1);
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , left  , thirdlyLineTop , left+ lineSizeBk , 0, 1);

        //循环晚
        thirdlyLineTop  = thirdlyLineTop+fontSizeZdm+jg*2;
        fourthlyLineTop = thirdlyLineTop+fontSizeZdm+jg*2;
        fourthlyLeft =  left;
        }

        //合计行
        LODOP.ADD_PRINT_LINE(thirdlyLineTop , left  , thirdlyLineTop , left+ lineSizeBk , 0, 1);


        //右边竖线
        LODOP.ADD_PRINT_LINE(lineTop , left+ lineSizeBk  , thirdlyLineTop , left+ lineSizeBk , 0, 1);



    }
  }

  //换行
  var fiveLineTop =  thirdlyLineTop+jg;

  LODOP.ADD_PRINT_TEXT(fiveLineTop+jg , left , lineSizeBk , fontSizeZdm, ("制单人："+ data.nickName+ "     "+"审核人："+data.auditName) );
  fiveLineTop = fiveLineTop+ fontSizeZdm+jg;

  LODOP.ADD_PRINT_TEXT(fiveLineTop+jg , left , lineSizeBk , fontSizeZdm, "销售单位："+ data.companyInfo.name );
  fiveLineTop =  fiveLineTop+ fontSizeZdm+jg;

  LODOP.ADD_PRINT_TEXT(fiveLineTop+jg , left , lineSizeBk , fontSizeZdm, "公司地址："+ data.companyInfo.address);
  fiveLineTop = fiveLineTop+ fontSizeZdm+jg;

  LODOP.ADD_PRINT_TEXT(fiveLineTop+jg , left , lineSizeBk , fontSizeZdm, "联系人："+ data.companyInfo.owner );
  fiveLineTop =  fiveLineTop+ fontSizeZdm+jg;

  LODOP.ADD_PRINT_TEXT(fiveLineTop+jg , left , lineSizeBk , fontSizeZdm, "联系电话:" +data.companyInfo.phone );


}



//打印不干胶
export function printsBGJ(data) {
    var fontSize = 12;
    var fontSize1 = 10
    var lineHeight0 = 25;
    var lineHeight1 = 20;
    var top  = 15;
    var left  = 10;
    LODOP.SET_PRINT_STYLE("FontName" ,"宋体");
    LODOP.SET_PRINT_STYLE("FontSize" ,fontSize);
    LODOP.SET_PRINT_STYLE("Bold" ,"1");
    LODOP.ADD_PRINT_TEXT(top,left,400,200,"挂点："+(data.mountPointName!=null?data.mountPointName:''));
    top+=lineHeight1;
    LODOP.ADD_PRINT_TEXT(top,left,400,200,"渠道名称："+(data.placeName!=null?data.placeName:''));
    top+=lineHeight0;
    LODOP.SET_PRINT_STYLE("FontSize" ,fontSize1);
    LODOP.ADD_PRINT_TEXT(top,left,400,200,"质检时间："+data.checkTime);
    top+=lineHeight1;
    LODOP.ADD_PRINT_TEXT(top,left,400,200,data.realName+"   "+data.phone);
    // top+=lineHeight1;
    // LODOP.ADD_PRINT_TEXT(top,left,400,200,"衣物编号："+data.clothesNo);
    top+=lineHeight1;
    LODOP.SET_PRINT_STYLE("Bold" ,"0");
    LODOP.ADD_PRINT_BARCODE(top,left,"60mm","18mm","128Auto",data.clothesNo);
}


//打印无纺布
export  function washPrint(data) {
    var fontSize = 7;
    var lineHeight = fontSize + 1;
    var top = 0;
    var left1 = 20;

    LODOP.SET_PRINT_STYLE('FontName', '宋体');
    LODOP.SET_PRINT_STYLE('FontSize', fontSize);

    LODOP.ADD_PRINT_TEXT(top, left1, 400, 200, data.placeTypeName + '：'+ data.placeName + '  【' + data.num + '】'+'  '+data.packTime+'  '+ (data.mountPoint!==null?data.mountPoint:''));
    top += lineHeight+3;
    LODOP.ADD_PRINT_LINE(top, left1, top, 600, 0, 1);

    top += 3;
    LODOP.ADD_PRINT_BARCODE(top, left1, '40mm', '10mm', '128Auto', data.clothesNum);
    lineHeight = lineHeight+1;

    let left3 = left1+160;
    LODOP.SET_PRINT_STYLE('FontSize', 9);
    LODOP.ADD_PRINT_TEXT(top, left3, 400, 200, data.userName + ' ' + data.phone + ' ' + data.clothesName);

    top += lineHeight+3;
    LODOP.ADD_PRINT_TEXT(
        top,
        left3,
        400,
        200,
        data.brandName + ';' + data.color + ' ;' + data.flaw + ' ;' + data.accessorialService + ' ;' + data.risk
    );
    top += lineHeight+3;
    LODOP.ADD_PRINT_TEXT(top, left3, 300, 200, data.accessory == null ? '' : '附件：' + data.accessory);
}

//打印小票
export function lodopPrint(printData) {
    var fontSize = 13.5;
    LODOP.SET_PRINT_STYLE('FontName', '宋体');
    LODOP.SET_PRINT_STYLE('FontSize', fontSize);
    LODOP.SET_PRINT_STYLE('Bold', '1');
    LODOP.ADD_PRINT_TEXT(1, 100, 100, 200, '洗衣凭据');
    fontSize = 10;
    LODOP.SET_PRINT_STYLE('FontSize', fontSize);
    LODOP.SET_PRINT_STYLE('Bold', '0');
    var lintDis = fontSize + 4;
    var top = 30;
    var left = 10;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 200, '时间: ' + printData.printTime);
    top += lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 200, '单号: ' + printData.orderNum);
    top += lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 200, '会员名称: ' + printData.uerName);
    top += lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 200, '会员电话: ' + printData.phone);
    top += lintDis;
    top += 5;
    LODOP.ADD_PRINT_LINE(top, left, top, 220, 3, 1);
    LODOP.SET_PRINT_STYLE('FontSize', 8);
    top += 5;
    let data = printData.clothesDtoList;
    for (var i = 1; i <= printData.clothesDtoList.length; i++) {
        LODOP.ADD_PRINT_TEXT(top, left - 10, 200, 10, i + '.');
        LODOP.ADD_PRINT_TEXT(top, left, 200, 200, '名称:' + data[i - 1].name);
        LODOP.ADD_PRINT_TEXT(top, left + 160, 200, 100, '颜色:' + data[i - 1].color);
        top += 15;
        LODOP.ADD_PRINT_TEXT(top, left, 200, 100, '服务类型:' + data[i - 1].serveType);
        LODOP.ADD_PRINT_TEXT(top, left + 160, 200, 100, '价格:' + data[i - 1].price);
        if(data[i-1].appendDto){
            if((data[i-1].appendDto.length)!=0){
                let appends = data[i-1].appendDto;
                for (var j=0 ;j<appends.length ;j++){
                    top += 15;
                    LODOP.ADD_PRINT_TEXT(top, left, 200, 100, '附加服务:' + appends[j].appendServiceName);
                    LODOP.ADD_PRINT_TEXT(top, left + 160, 200, 100, '价格:' + appends[j].appendServiceAmount);
                }

            }
        }
        top += 15;
        LODOP.ADD_PRINT_TEXT(top, left, 200, 100, '@瑕疵(:' + data[i - 1].flaw + ')');
        var e = Math.ceil((('@瑕疵(:' + data[i - 1].flaw + ')').length)/19)
        var flawHeight = 14*e;
        top += flawHeight;
    }
    LODOP.SET_PRINT_STYLE('FontSize', fontSize);

    top += lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 100, '总件数: ' + printData.sumPiece);
    top += lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 100, '总金额: ' + printData.sumPrice);
    top += lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 100, '会员折扣: ' + printData.currDiscount);
    top += lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 100, '免收金额: ' + printData.derateAmount);
    top += lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 100, '支付金额: ' + printData.payAmount);
    top += lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 100, '支付方式: ' + printData.payType);
    top += lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 100, '账户余额: ' + printData.balance);
    top += lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 100, '支付状态: ' + printData.isPay);
    top += lintDis;
    top += 5;
    LODOP.ADD_PRINT_LINE(top, left, top, 220, 3, 1);
    top += 5;
    LODOP.ADD_PRINT_TEXT(top, left, 220, 200, '店铺地址: ' + printData.shopAddress);
    top += 4 * lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 100, '服务热线: ' + printData.servePhone);
    top += lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 100, '店员: ' + printData.staffNum);
    top += lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 200, 100, '感谢您的使用!');
    top += lintDis;
    LODOP.ADD_PRINT_TEXT(top, left, 220, 100, '*****请妥善保管此凭证*****');
}



var CreatedOKLodopObject, CLodopIsLocal, CLodopJsState;

//==判断是否需要CLodop(那些不支持插件的浏览器):==
export function needCLodop() {
    try {
        var ua = navigator.userAgent;
        if (ua.match(/Windows\sPhone/i)) return true;
        if (ua.match(/iPhone|iPod|iPad/i)) return true;
        if (ua.match(/Android/i)) return true;
        if (ua.match(/Edge\D?\d+/i)) return true;

        var verTrident = ua.match(/Trident\D?\d+/i);
        var verIE = ua.match(/MSIE\D?\d+/i);
        var verOPR = ua.match(/OPR\D?\d+/i);
        var verFF = ua.match(/Firefox\D?\d+/i);
        var x64 = ua.match(/x64/i);
        if (!verTrident && !verIE && x64) return true;
        else if (verFF) {
            verFF = verFF[0].match(/\d+/);
            if (verFF[0] >= 41 || x64) return true;
        } else if (verOPR) {
            verOPR = verOPR[0].match(/\d+/);
            if (verOPR[0] >= 32) return true;
        } else if (!verTrident && !verIE) {
            var verChrome = ua.match(/Chrome\D?\d+/i);
            if (verChrome) {
                verChrome = verChrome[0].match(/\d+/);
                if (verChrome[0] >= 41) return true;
            }
        }
        return false;
    } catch (err) {
        return true;
    }
}

//==加载引用CLodop的主JS,用双端口8000和18000(以防其中一个被占):==
export function loadCLodop() {
    if (CLodopJsState == 'loading' || CLodopJsState == 'complete') return;
    CLodopJsState = 'loading';
    var head = document.head || document.getElementsByTagName('head')[0] || document.documentElement;
    var JS1 = document.createElement('script');
    var JS2 = document.createElement('script');
    JS1.src = `${baseURL}/CLodopfuncs.js?priority=1`;
    JS2.src = `${baseURL}/CLodopfuncs.js`;
    // alert("...."+`${baseURL}/CLodopfuncs.js?priority=1`);
    JS1.onload = JS2.onload = function () {
        CLodopJsState = 'complete';
    };
    JS1.onerror = JS2.onerror = function (evt) {
        CLodopJsState = 'complete';
    };
    head.insertBefore(JS1, head.firstChild);
    head.insertBefore(JS2, head.firstChild);
    CLodopIsLocal = !!(JS1.src + JS2.src).match(/\/\/localho|\/\/127.0.0./i);
}

if (needCLodop()) {
    loadCLodop();
} //加载

//==获取LODOP对象主过程,判断是否安装、需否升级:==

export function getLodop(oOBJECT, oEMBED) {
    var strHtmInstall =
        "<br><font color='#FF00FF'>打印控件未安装!点击这里<a href='install_lodop32.zip' target='_self'>执行安装</a>,安装后请刷新页面或重新进入。</font>";
    var strHtmUpdate =
        "<br><font color='#FF00FF'>打印控件需要升级!点击这里<a href='install_lodop32.zip' target='_self'>执行升级</a>,升级后请重新进入。</font>";
    var strHtm64_Install =
        "<br><font color='#FF00FF'>打印控件未安装!点击这里<a href='install_lodop64.zip' target='_self'>执行安装</a>,安装后请刷新页面或重新进入。</font>";
    var strHtm64_Update =
        "<br><font color='#FF00FF'>打印控件需要升级!点击这里<a href='install_lodop64.zip' target='_self'>执行升级</a>,升级后请重新进入。</font>";
    var strHtmFireFox =
        "<br><br><font color='#FF00FF'>（注意：如曾安装过Lodop旧版附件npActiveXPLugin,请在【工具】->【附加组件】->【扩展】中先卸它）</font>";
    var strHtmChrome = "<br><br><font color='#FF00FF'>(如果此前正常，仅因浏览器升级或重安装而出问题，需重新执行以上安装）</font>";
    var strCLodopInstall_1 =
        "<br><font color='#FF00FF'>Web打印服务CLodop未安装启动，点击这里<a href='CLodop_Setup_for_Win32NT.zip' target='_self'>下载执行安装</a>";
    var strCLodopInstall_2 = "<br>（若此前已安装过，可<a href='CLodop.protocol:setup' target='_self'>点这里直接再次启动</a>）";
    var strCLodopInstall_3 = '，成功后请刷新本页面。</font>';
    var strCLodopUpdate =
        "<br><font color='#FF00FF'>Web打印服务CLodop需升级!点击这里<a href='CLodop_Setup_for_Win32NT.zip' target='_self'>执行升级</a>,升级后请刷新页面。</font>";
    var LODOP;
    var ua = navigator.userAgent;
    var isIE = !!ua.match(/MSIE/i) || !!ua.match(/Trident/i);

    if (needCLodop()) {
        try {
            LODOP = getCLodop();
        } catch (err) {
            Message.warning({
                message: 'Web打印服务CLodop未安装启动'
            });
            return;
        }
        if (!LODOP && CLodopJsState !== 'complete') {
            if (CLodopJsState == 'loading') alert('网页还没下载完毕，请稍等一下再操作.');
            else alert('没有加载CLodop的主js，请先调用loadCLodop过程.');
            return;
        }
        if (!LODOP) {
            Message.warning({
                message: 'Web打印服务CLodop未安装启动'
            });
            return;
        } else {
            if (CLODOP.CVERSION < '4.0.9.9') {
                Message.warning({
                    message: 'Web打印服务CLodop需升级!'
                });
                // document.body.innerHTML = strCLodopUpdate + document.body.innerHTML;
            }
            if (oEMBED && oEMBED.parentNode) oEMBED.parentNode.removeChild(oEMBED);
            if (oOBJECT && oOBJECT.parentNode) oOBJECT.parentNode.removeChild(oOBJECT);
        }
    } else {
        var is64IE = isIE && !!ua.match(/x64/i);
        //==如果页面有Lodop就直接使用,否则新建:==
        if (oOBJECT || oEMBED) {
            if (isIE) LODOP = oOBJECT;
            else LODOP = oEMBED;
        } else if (!CreatedOKLodopObject) {
            LODOP = document.createElement('object');
            LODOP.setAttribute('width', 0);
            LODOP.setAttribute('height', 0);
            LODOP.setAttribute('style', 'position:absolute;left:0px;top:-100px;width:0px;height:0px;');
            if (isIE) LODOP.setAttribute('classid', 'clsid:2105C259-1E0C-4534-8141-A753534CB4CA');
            else LODOP.setAttribute('type', 'application/x-print-lodop');
            document.documentElement.appendChild(LODOP);
            CreatedOKLodopObject = LODOP;
        } else LODOP = CreatedOKLodopObject;
        //==Lodop插件未安装时提示下载地址:==
        // if (!LODOP || !LODOP.VERSION) {
        //     if (ua.indexOf('Chrome') >= 0) this.$message.error(`${strHtmChrome}`);
        //     if (ua.indexOf('Firefox') >= 0) this.$message.error(`${strHtmFireFox}`);
        //     if(is64IE) {
        //         this.$message.error(`${strHtm64_Install}`);
        //     } else {
        //         this.$message.error(`${strHtmInstall}`);
        //     }
        //     return LODOP;
        // }
    }
    if (LODOP.VERSION < '6.2.2.6') {
        if (!needCLodop()) document.body.innerHTML = (is64IE ? strHtm64_Update : strHtmUpdate) + document.body.innerHTML;
    }
    //===如下空白位置适合调用统一功能(如注册语句、语言选择等):==
    LODOP.SET_LICENSES('', '13528A153BAEE3A0254B9507DCDE2839', 'EDE92F75B6A3D917F65910', '');

    return LODOP;
}
