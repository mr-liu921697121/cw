import request from '@/utils/request';

export function findByType(params) {
  return request({
    url: '/api/system/config/findByType',
    method: 'post',
    params: params,
  });
}


export function findTypeList(params) {
  return request({
    url: '/api/system/submPrice/findList',
    method: 'post',
    params: params,
  });
}