import request from '@/utils/request';

export function findPage(query) {
  return request({
    url: '/api/system/project/findPage',
    method: 'get',
    params: query
  });
}

export function auditPage(query) {
  return request({
    url: '/api/system/project/auditPage',
    method: 'post',
    params: query
  });
}

export function saveProject(params) {
  return request({
    url: '/api/system/project/save',
    method: 'post',
    data: params
  });
}

export function deletePro(params) {
  return request({
    url: '/api/system/project/deletePro',
    method: 'post',
    data: params
  });
}
//提交流程
export function start(params) {
  return request({
    url: '/api/system/project/start',
    method: 'post',
    data: params
  });
}
//审批流程
export function audit(params) {
  return request({
    url: '/api/system/project/audit',
    method: 'post',
    data: params
  });
}

// 审核记录
export function findCommentByProId(params) {
  return request({
    url: '/api/system/project/findCommentByProId',
    method: 'post',
    params: params
  });
}

// 加工订单列表  orderStatus  1      订单出库列表    orderStatus  2
export function processOrStockPage(params) {
  return request({
    url: '/api/system/project/processOrStockPage',
    method: 'post',
    params: params
  });
}


// 订单出库
export function stockOut(params) {
  return request({
    url: '/api/system/project/stockOut',
    method: 'post',
    data: params
  });
}

// 订单管理
export function orderPage(params) {
  return request({
    url: '/api/system/project/orderPage',
    method: 'post',
    data: params
  });
}


// 打印
export function print(params) {
  return request({
    url: '/api/system/project/print',
    method: 'post',
    data: params
  });
}

// 导出数据  
export function exportProjectInfoById(params) {
  return request({
    url: '/api/system/project/exportProjectInfoById',
    method: 'post',
    params: params,
    responseType: "blob"
  });
}

// 辅材列表
export function submPricePage(params) {
  return request({
    url: '/api/system/submPrice/findPage',
    method: 'post',
    params: params
  });
}

// 辅材-新增，修改
export function submPriceSave(params) {
  return request({
    url: '/api/system/submPrice/save',
    method: 'post',
    data: params
  });
}

// 辅材-删除
export function submPriceDelete(params) {
  return request({
    url: '/api/system/submPrice/delete',
    method: 'post',
    params: params
  });
}

// 获取当前日期范围
export function getNowDate() {
  var now = new Date()
  var year = now.getFullYear()
  var month = now.getMonth()
  var date = now.getDate()
  month = month + 1
  month = month.toString().padStart(2, '0')
  date = date.toString().padStart(2, '0')
  var defaultDate = `${year}-${month}-${date}`
  return defaultDate
}