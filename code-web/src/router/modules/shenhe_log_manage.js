import Layout from '@/views/layout/Layout'

const shenheLogManage = {
  path: '/shenheLogManage',
  component: Layout,
  alwaysShow: true,
  name: '审核记录',
  redirect: 'noredirect',
  meta: {
    title: '审核记录',
    icon: 'component',
    resources: 'shenheLog'
  },
  children: [
    {
      path: 'shenheLog',
      component: () => import('@/views/shenheLog_manage/shenheLog'),
      name: '审核记录',
      meta: { title: '审核记录', icon: 'my-sysmenu', noCache: true, resources: 'shenheLog' }
    }
  ]
}

export default shenheLogManage
