import Layout from '@/views/layout/Layout'

const bjdManage = {
  path: '/bjdManage',
  component: Layout,
  alwaysShow: true,
  name: '报价单',
  redirect: 'noredirect',
  meta: {
    title: '报价单',
    icon: 'component',
    resources: 'bjd'
  },
  children: [
    {
      path: 'dxfgbj',
      component: () => import('@/views/bjd-manage/dxfgbj'),
      name: '镀锌风管报价',
      meta: { title: '镀锌风管报价', icon: 'my-sysmenu', noCache: true, resources: 'dxfgbj' }
    },
    {
      path: 'yflbjd',
      component: () => import('@/views/bjd-manage/yflbjd'),
      name: '园法兰报价',
      meta: { title: '园法兰报价', icon: 'my-sysmenu', noCache: true, resources: 'yflbjd' }
    }
  ]
}

export default bjdManage
