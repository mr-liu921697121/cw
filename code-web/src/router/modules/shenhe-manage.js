import Layout from '@/views/layout/Layout'

const shenheManage = {
  path: '/shenheManage',
  component: Layout,
  alwaysShow: true,
  name: '审核订单',
  redirect: 'noredirect',
  meta: {
    title: '审核订单',
    icon: 'component',
    resources: 'shenhe'
  },
  children: [
    {
      path: 'shenhe',
      component: () => import('@/views/shenhe-manage/shenhe'),
      name: '审核订单',
      meta: { title: '审核订单', icon: 'my-sysmenu', noCache: true, resources: 'shenhe' }
    }
  ]
}

export default shenheManage
