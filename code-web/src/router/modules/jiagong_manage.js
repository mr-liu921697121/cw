import Layout from '@/views/layout/Layout'

const jiagongManage = {
  path: '/jiagongManage',
  component: Layout,
  alwaysShow: true,
  name: '加工订单',
  redirect: 'noredirect',
  meta: {
    title: '加工订单',
    icon: 'component',
    resources: 'jiagong'
  },
  children: [
    {
      path: 'jiagong',
      component: () => import('@/views/jiagong_manage/jiagong'),
      name: '加工订单',
      meta: { title: '加工订单', icon: 'my-sysmenu', noCache: true, resources: 'jiagong' }
    }
  ]
}

export default jiagongManage
