import Layout from '@/views/layout/Layout'

const chukuManage = {
  path: '/chukuManage',
  component: Layout,
  alwaysShow: true,
  name: '出库订单',
  redirect: 'noredirect',
  meta: {
    title: '出库订单',
    icon: 'component',
    resources: 'chuku'
  },
  children: [
    {
      path: 'chuku',
      component: () => import('@/views/chuku_manage/chuku'),
      name: '出库订单',
      meta: { title: '出库订单', icon: 'my-sysmenu', noCache: true, resources: 'chuku' }
    }
  ]
}

export default chukuManage
