import Layout from '@/views/layout/Layout'

const orderManage = {
  path: '/orderManage',
  component: Layout,
  alwaysShow: true,
  name: '订单管理',
  redirect: 'noredirect',
  meta: {
    title: '订单管理',
    icon: 'component',
    resources: 'order'
  },
  children: [
    {
      path: 'order',
      component: () => import('@/views/order_manage/order'),
      name: '订单管理',
      meta: { title: '订单管理', icon: 'my-sysmenu', noCache: true, resources: 'order' }
    }
  ]
}

export default orderManage
