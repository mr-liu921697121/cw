import Layout from '@/views/layout/Layout'

const auxiliaryManage = {
  path: '/auxiliary',
  component: Layout,
  alwaysShow: true,
  name: '辅材管理',
  redirect: 'noredirect',
  meta: {
    title: '辅材管理',
    icon: 'component',
    resources: 'auxiliary'
  },
  children: [
    {
      path: 'auxiliary',
      component: () => import('@/views/auxiliary-manage/auxiliary'),
      name: '辅材管理',
      meta: { title: '辅材管理', icon: 'my-sysmenu', noCache: true, resources: 'auxiliary' }
    }
  ]
}

export default auxiliaryManage
