package com.zhengqing.modules.common.api;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 *  <p> 基类 </p>
 *
 * @description:
 * @author: zhengqing
 * @date: 2019/8/17 0017 19:53
 */
@RestController
@Slf4j
public class BaseController {

    public Logger LOG = LoggerFactory.getLogger( getClass() );


    public Page getPage(HttpServletRequest request)
    {
        int pageNo = request.getParameter("page")==null?1:Integer.valueOf(request.getParameter("page"));
        int pageSize = request.getParameter("limit")==null?10:Integer.valueOf(request.getParameter("limit"));
        Page page = new Page<>(pageNo, pageSize);
        return page;
    }

}
