package com.zhengqing.modules.common.runner;

import com.zhengqing.modules.activiti.IFlowTaskService;
import com.zhengqing.modules.common.api.EnumConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class FlawDeployRunner implements CommandLineRunner {

    @Autowired
    private IFlowTaskService flowTaskService;

    public void run(String... args) throws Exception{
        List<Map<String , String>> list = new ArrayList<>();
        Map<String , String> map1 = new HashMap<>();
        map1.put("flowName" , EnumConfig.FLOW_NAME_PROJECT_TYPE_YFL.value);
        map1.put("filePath" , "processes/yflFlow.bpmn");
        map1.put("refreshFlag" , "1");

        Map<String , String> map2 = new HashMap<>();
        map2.put("flowName" , EnumConfig.FLOW_NAME_PROJECT_TYPE_DXFG.value);
        map2.put("filePath" , "processes/dxfgFlow.bpmn");
        map2.put("refreshFlag" , "1");

        list.add(map1);
        list.add(map2);
        for (Map map: list) {
            flowTaskService.deploy2(map);
        }
    }

    public static void main(String[] args){
        Map<String,String> treeMap = new TreeMap<>();
        String rsaDecrypt= "a=aaa&b=bbbbb==";
        System.out.println(".........."+rsaDecrypt.indexOf("="));
    }

}
