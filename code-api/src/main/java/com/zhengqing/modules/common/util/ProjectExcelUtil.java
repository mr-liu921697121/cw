package com.zhengqing.modules.common.util;

import com.alibaba.fastjson.JSONArray;
import com.zhengqing.modules.common.api.EnumConfig;
import com.zhengqing.modules.common.codec.EncodeUtils;
import com.zhengqing.modules.project.vo.ProjectDxfgChild;
import com.zhengqing.modules.project.vo.ProjectYflChild;
import com.zhengqing.modules.project.vo.TSysProjectVo;
import com.zhengqing.modules.system.entity.SysConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.List;

public class ProjectExcelUtil {

//    public static void exportExcel(HttpServletResponse response , List<SysConfig> configList , TSysProjectVo vo){
//        ExcelExport ee = null;
//        String fileName = "";
//        if (vo.getProjectType().equals(EnumConfig.PROJECT_TYPE_DXFG.value)){
//            // 镀锌风管
//            List<ProjectDxfgChild> list = JSONArray.parseArray(vo.getContextJson() , ProjectDxfgChild.class);
//            String title = "客户："+vo.getClientName()+"    项目："+vo.getProjectName()+"    电话："+vo.getPhone()+"    报价日期："+vo.getMakeDate()
//                    +"    是否含税："+EnumConfig.getName("SYS_YES_OR_NO" , vo.getPluxDutyFlag())+"    是否含运费："
//                    +EnumConfig.getName("SYS_YES_OR_NO" , vo.getFreightFlag());
//            ee = new ExcelExport("镀锌风管",title , ProjectDxfgChild.class , ExcelField.Type.EXPORT,null);
//            ee.setDataList(list);
//            fileName = "镀锌风管-"+vo.getProjectName()+".xlsx";
//        }else {
//            // 圆法兰
//            List<ProjectYflChild> list = JSONArray.parseArray(vo.getContextJson() , ProjectYflChild.class);
//            String title = "客户："+vo.getClientName()+"    项目："+vo.getProjectName()+"    电话："+vo.getPhone()+"    报价日期："+vo.getMakeDate()
//                    +"    是否含税："+EnumConfig.getName("SYS_YES_OR_NO" , vo.getPluxDutyFlag())+"    是否含运费："
//                    +EnumConfig.getName("SYS_YES_OR_NO" , vo.getFreightFlag());
//            ee = new ExcelExport("圆法兰",title , ProjectYflChild.class , ExcelField.Type.EXPORT,null);
//            ee.setDataList(list);
//            fileName = "圆法兰-"+vo.getProjectName()+".xlsx";
//        }
//        Row zbrRow = ee.addRow();
//        ee.addCell(zbrRow , 0 , "制表人");
//        ee.addCell(zbrRow , 1 , vo.getNickName());
//
//        Row bjdwRow = ee.addRow();
//        ee.addCell(bjdwRow , 0 , "报价单位");
//        ee.addCell(bjdwRow , 1 , configList.get(0).getParamValue());
//
//        Row gsdzRow = ee.addRow();
//        ee.addCell(gsdzRow , 0 , "公司地址");
//        ee.addCell(gsdzRow , 1 , configList.get(1).getParamValue());
//
//        Row lxdhRow = ee.addRow();
//        ee.addCell(lxdhRow , 0 , "联系电话");
//        ee.addCell(lxdhRow , 1 , configList.get(2).getParamValue());
//
//        Row lxrRow = ee.addRow();
//        ee.addCell(lxrRow , 0 , "联系人");
//        ee.addCell(lxrRow , 1 , configList.get(3).getParamValue());
//
//        try{
//            ee.write(response , fileName);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

    public  void exportExcel(HttpServletResponse response , List<SysConfig> configList , TSysProjectVo vo){
        //创建一个工作簿，也就是Excel文件
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet();

        HSSFCellStyle allStyle = wb.createCellStyle();
        allStyle.setBorderBottom(BorderStyle.THIN);
        allStyle.setBorderTop(BorderStyle.THIN);
        allStyle.setBorderLeft(BorderStyle.THIN);
        allStyle.setBorderRight(BorderStyle.THIN);

        HSSFRow row = sheet.createRow(0);
        HSSFCell cell = row.createCell(0);
        String fileName = "";
        int rowInt = 2;
        int rangeCell = 0;
        BigDecimal areaCount = BigDecimal.ZERO;
        BigDecimal amountCount = BigDecimal.ZERO;
        if (vo.getProjectType().equals(EnumConfig.PROJECT_TYPE_DXFG.value)){
            rangeCell = 11;
            // 镀锌风管
            List<ProjectDxfgChild> list = JSONArray.parseArray(vo.getContextJson() , ProjectDxfgChild.class);
            StringBuffer sbTitle = new StringBuffer();
            sbTitle.append("客户：");
            sbTitle.append(vo.getClientName()==null?"":vo.getClientName());
            sbTitle.append("    项目：");
            sbTitle.append(vo.getProjectName()==null?"":vo.getProjectName());
            sbTitle.append("    电话：");
            sbTitle.append(vo.getPhone()==null?"":vo.getPhone());
            sbTitle.append("    报价日期：");
            sbTitle.append(vo.getMakeDate());
            sbTitle.append("    是否含税：");
            sbTitle.append(EnumConfig.getName("SYS_YES_OR_NO" , vo.getPluxDutyFlag()));
            sbTitle.append("    是否含运费：");
            sbTitle.append(EnumConfig.getName("SYS_YES_OR_NO" , vo.getFreightFlag()));
//            String title = "客户："+vo.getClientName()==null?"":vo.getClientName()+"    项目："+vo.getProjectName()==null?"":vo.getProjectName()+"    电话："+vo.getPhone()==null?"":vo.getPhone()+"    报价日期："+vo.getMakeDate()
//                    +"    是否含税："+EnumConfig.getName("SYS_YES_OR_NO" , vo.getPluxDutyFlag())+"    是否含运费："
//                    +EnumConfig.getName("SYS_YES_OR_NO" , vo.getFreightFlag());
            fileName = "镀锌风管-"+vo.getProjectName()+".xlsx";
            cell.setCellValue(sbTitle.toString());
            CellRangeAddress row1Range = new CellRangeAddress(0 , 0 , 0,11);
            sheet.addMergedRegion(row1Range);
            Row row1 = sheet.createRow(1);
            Cell row1Cell0 = row1.createCell(0);
            row1Cell0.setCellValue("产品名称");
            Cell row1Cell1 = row1.createCell(1);
            row1Cell1.setCellValue("型号");
            Cell row1Cell2 = row1.createCell(2);
            row1Cell2.setCellValue("长");
            Cell row1Cell3 = row1.createCell(3);
            row1Cell3.setCellValue("宽");
            Cell row1Cell4 = row1.createCell(4);
            row1Cell4.setCellValue("长度");
            Cell row1Cell5 = row1.createCell(5);
            row1Cell5.setCellValue("单位");
            Cell row1Cell6 = row1.createCell(6);
            row1Cell6.setCellValue("数量");
            Cell row1Cell7 = row1.createCell(7);
            row1Cell7.setCellValue("单个面积");
            Cell row1Cell8 = row1.createCell(8);
            row1Cell8.setCellValue("总面积");
            Cell row1Cell9 = row1.createCell(9);
            row1Cell9.setCellValue("单价");
            Cell row1Cell10 = row1.createCell(10);
            row1Cell10.setCellValue("总价");
            Cell row1Cell11 = row1.createCell(11);
            row1Cell11.setCellValue("备注");

            for (ProjectDxfgChild c: list) {
                HSSFRow dataRow = sheet.createRow(rowInt++);
                Cell dataRowCell0 = dataRow.createCell(0);
                dataRowCell0.setCellValue(c.getProductName());
                Cell dataRowCell1 = dataRow.createCell(1);
                dataRowCell1.setCellValue(c.getModel());
                Cell dataRowCell2 = dataRow.createCell(2);
                dataRowCell2.setCellValue(c.getLong1());
                Cell dataRowCell3 = dataRow.createCell(3);
                dataRowCell3.setCellValue(c.getWide());
                Cell dataRowCell4 = dataRow.createCell(4);
                dataRowCell4.setCellValue(c.getLength());
                Cell dataRowCell5 = dataRow.createCell(5);
                dataRowCell5.setCellValue(c.getUnit());
                Cell dataRowCell6 = dataRow.createCell(6);
                dataRowCell6.setCellValue(c.getQuantity());
                Cell dataRowCell7 = dataRow.createCell(7);
                dataRowCell7.setCellValue(c.getArea());
                Cell dataRowCell8 = dataRow.createCell(8);
                dataRowCell8.setCellValue(c.getTotalArea());
                Cell dataRowCell9 = dataRow.createCell(9);
                dataRowCell9.setCellValue(c.getUnitPrice());
                Cell dataRowCell10 = dataRow.createCell(10);
                dataRowCell10.setCellValue(c.getTotalPrices());
                Cell dataRowCell11 = dataRow.createCell(11);
                dataRowCell11.setCellValue(c.getRemark());
                if (StringUtils.isNotEmpty(c.getTotalArea())){
                    areaCount = areaCount.add(new BigDecimal(c.getTotalArea()));
                }
                if (StringUtils.isNotEmpty(c.getTotalPrices())){
                    amountCount = amountCount.add(new BigDecimal(c.getTotalPrices()));
                }
            }
            HSSFRow countRow = sheet.createRow(rowInt++);
            Cell countCell0 = countRow.createCell(0);
            countCell0.setCellValue("合计");
            Cell countCell8 = countRow.createCell(8);
            countCell8.setCellValue(areaCount.toString());
            Cell countCell10 = countRow.createCell(10);
            countCell10.setCellValue(amountCount.toString());
        }else {
            rangeCell = 7;
            // 圆法兰
            List<ProjectYflChild> list = JSONArray.parseArray(vo.getContextJson() , ProjectYflChild.class);
            StringBuffer sbTitle = new StringBuffer();
            sbTitle.append("客户：");
            sbTitle.append(vo.getClientName()==null?"":vo.getClientName());
            sbTitle.append("    项目：");
            sbTitle.append(vo.getProjectName()==null?"":vo.getProjectName());
            sbTitle.append("    电话：");
            sbTitle.append(vo.getPhone()==null?"":vo.getPhone());
           // String title = "客户："+vo.getClientName()==null?"":vo.getClientName()+"        项目："+vo.getProjectName()==null?"":vo.getProjectName()+"        电话："+vo.getPhone()==null?"":vo.getPhone();
            String title2 = "报价日期："+vo.getMakeDate()
                    +"        是否含税："+EnumConfig.getName("SYS_YES_OR_NO" , vo.getPluxDutyFlag())+"        是否含运费："
                    +EnumConfig.getName("SYS_YES_OR_NO" , vo.getFreightFlag());
            fileName = "圆法兰-"+vo.getProjectName()+".xlsx";
            cell.setCellValue(sbTitle.toString());
            Row titlerow = sheet.createRow(1);
            Cell cellTitle2 = titlerow.createCell(0);
            cellTitle2.setCellValue(title2);
            CellRangeAddress row1Range = new CellRangeAddress(0 , 0 , 0,6);
            sheet.addMergedRegion(row1Range);
            CellRangeAddress titleRow1Range = new CellRangeAddress(1 , 1 , 0,6);
            sheet.addMergedRegion(titleRow1Range);
            Row row1 = sheet.createRow(2);
            Cell row1Cell0 = row1.createCell(0);
            row1Cell0.setCellValue("产品名称");
            Cell row1Cell1 = row1.createCell(1);
            row1Cell1.setCellValue("型号");
            Cell row1Cellnzj = row1.createCell(2);
            row1Cellnzj.setCellValue("内直径");

            Cell row1Cell2 = row1.createCell(3);
            row1Cell2.setCellValue("数量");
            Cell row1Cell3 = row1.createCell(4);
            row1Cell3.setCellValue("孔数量");
            Cell row1Cell4 = row1.createCell(5);
            row1Cell4.setCellValue("单价");
            Cell row1Cell5 = row1.createCell(6);
            row1Cell5.setCellValue("总价");
            Cell row1Cell6 = row1.createCell(7);
            row1Cell6.setCellValue("备注");
            rowInt = 3;
            for (ProjectYflChild c: list) {
                HSSFRow dataRow = sheet.createRow(rowInt++);
                Cell dataRowCell0 = dataRow.createCell(0);
                dataRowCell0.setCellValue(c.getProductName());
                Cell dataRowCell1 = dataRow.createCell(1);
                dataRowCell1.setCellValue(c.getModel());

                Cell dataRownzj = dataRow.createCell(2);
                dataRownzj.setCellValue(c.getInnerDiameter());

                Cell dataRowCell2 = dataRow.createCell(3);
                dataRowCell2.setCellValue(c.getQuantity());
                Cell dataRowCell3 = dataRow.createCell(4);
                dataRowCell3.setCellValue(c.getHoleCount());
                Cell dataRowCell4 = dataRow.createCell(5);
                dataRowCell4.setCellValue(c.getUnitPrice());
                Cell dataRowCell5 = dataRow.createCell(6);
                dataRowCell5.setCellValue(c.getTotalPrices());
                Cell dataRowCell6 = dataRow.createCell(7);
                dataRowCell6.setCellValue(c.getRemark());

                if (StringUtils.isNotEmpty(c.getTotalPrices())){
                    amountCount = amountCount.add(new BigDecimal(c.getTotalPrices()));
                }
            }
            HSSFRow countRow = sheet.createRow(rowInt++);
            Cell countCell0 = countRow.createCell(0);
            countCell0.setCellValue("合计");
            Cell countCell10 = countRow.createCell(5);
            countCell10.setCellValue(amountCount.toString());
        }

        // TODO 红色话术相关
        rowInt = rowInt+1;
        Row huashu1 = sheet.createRow(rowInt-1);
        Cell huashu1Cell = huashu1.createCell(0); //.setCellValue("此报价有效期2天，价格随市场材料涨幅比例调价"
        CellStyle huashu1Style = wb.createCellStyle();
        Font redFont = wb.createFont();
        redFont.setColor(Font.COLOR_RED);
        huashu1Style.setFont(redFont);
        huashu1Style.setAlignment(HorizontalAlignment.CENTER); // 设置文字居中
        huashu1Cell.setCellStyle(huashu1Style);
        CellRangeAddress huashuRow1Range = new CellRangeAddress(rowInt-1 , rowInt-1 , 0,rangeCell);
        sheet.addMergedRegion(huashuRow1Range);
        huashu1Cell.setCellValue("此报价有效期2天，价格随市场材料涨幅比例调价");

        // TODO下面黑色话术相关
        rowInt = rowInt+1;
        Row huashu2 = sheet.createRow(rowInt-1);
        huashu2.setHeightInPoints(50);
        Cell huashu2Cell = huashu2.createCell(0); //.setCellValue("此报价有效期2天，价格随市场材料涨幅比例调价"
        CellRangeAddress huashu2Row1Range = new CellRangeAddress(rowInt-1 , rowInt-1 , 0,rangeCell);
        sheet.addMergedRegion(huashu2Row1Range);
        CellStyle autoStyle = wb.createCellStyle();
        autoStyle.setWrapText(true);
        huashu2Cell.setCellValue("温馨提示：1.请客户认真核对以上明细，如有异议请致电厂家。 \n2.付款方式：下单预付定金40%开始生产，生产完毕货款付清安排发货。\n3.计算方式：标准节直管面积以风管实际用料计算，异形管安电脑排版图计算，角铁法兰按有效长度计算。");
        huashu2Cell.setCellStyle(autoStyle);

        Row zbrRow = sheet.createRow(rowInt++);
        zbrRow.createCell( 0 ).setCellValue("制表人");
        zbrRow.createCell(1).setCellValue(vo.getNickName());

        Row bjdwRow = sheet.createRow(rowInt++);
        bjdwRow.createCell( 0 ).setCellValue("报价单位");
        bjdwRow.createCell( 1 ).setCellValue(configList.get(0).getParamValue());

        Row gsdzRow = sheet.createRow(rowInt++);
        gsdzRow.createCell( 0 ).setCellValue("公司地址");
        gsdzRow.createCell( 1 ).setCellValue(configList.get(1).getParamValue());

        Row lxdhRow = sheet.createRow(rowInt++);
        lxdhRow.createCell( 0 ).setCellValue("联系人");
        lxdhRow.createCell( 1 ).setCellValue(configList.get(2).getParamValue());

        Row lxrRow = sheet.createRow(rowInt++);
        lxrRow.createCell( 0 ).setCellValue("联系电话");
        lxrRow.createCell( 1 ).setCellValue(configList.get(3).getParamValue());
        OutputStream os = null;
        try {
            os = response.getOutputStream();
            fileName = new String(fileName.getBytes(),"UTF-8");
            response.setHeader("Content-Disposition", "attachment; filename="+EncodeUtils.encodeUrl(fileName));
            response.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN,"*");
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            response.addHeader("Access-Control-Allow-Headers", "Content-Type");
            response.setContentType("application/vnd.ms-excel; charset=utf-8");  //application/octet-stream; charset=utf-8
            wb.write(os);
            os.flush();
            wb.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }finally {
            if (os!=null){
                try{
                    os.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
}
