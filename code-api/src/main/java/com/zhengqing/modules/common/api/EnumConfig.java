package com.zhengqing.modules.common.api;

public enum EnumConfig {

    // 删除
    DEL_FLAG_NOT_DELETE("未删除" , "DEL_FLAG" , "0"),
    DEL_FLAG_DELETE("已删除" , "DEL_FLAG" , "1"),

    // 超时
    SYS_YES("是" , "SYS_YES_OR_NO" , "1"),
    SYS_NO("否" , "SYS_YES_OR_NO" , "0"),

    // 项目类型
    PROJECT_TYPE_DXFG("镀锌风管" , "PROJECT_TYPE" , "1"),
    PROJECT_TYPE_YFL("圆法兰" , "PROJECT_TYPE" , "2"),

    FLOW_STATUS_CREATE("待提交","FLOW_STATUS","0"),
    FLOW_STATUS_AUDIT("审批中","FLOW_STATUS","1"),
    FLOW_STATUS_RETURN("驳回","FLOW_STATUS","2"),
    FLOW_STATUS_PASS("审核通过","FLOW_STATUS","3"),


    PROJECT_ORDER_STATUS_0("审批未完成","PROJECT_ORDER_STATUS","0"),
    PROJECT_ORDER_STATUS_1("加工","PROJECT_ORDER_STATUS","1"),
    PROJECT_ORDER_STATUS_2("已出库","PROJECT_ORDER_STATUS","2"),

    FLOW_COMMENT_STATUS_0("待审核" , "FLOW_COMMENT_STATUS" , "0"),
    FLOW_COMMENT_STATUS_1("同意" , "FLOW_COMMENT_STATUS" , "1"),
    FLOW_COMMENT_STATUS_2("退回" , "FLOW_COMMENT_STATUS" , "2"),

    FLOW_NAME_PROJECT_TYPE_DXFG("镀锌风管" , "FLOW_NAME" , "dxfg_flow"),
    FLOW_NAME_PROJECT_TYPE_YFL("圆法兰" , "FLOW_NAME" , "yfl_flow"),

    FLOW_NAME_TEST1("圆法兰","FLOW_NAME","test1_start");

    public String name;

    public String desc;

    public String value;

    private EnumConfig(String name , String desc , String value) {
        this.value = value;
        this.desc = desc;
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public String getName() {
        return name;
    }

    public static String getName(String desc , String value){
        for(EnumConfig c : EnumConfig.values()){
            if (c.desc.equals(desc) && c.value.equals(value)){
                return c.name;
            }
        }
        return null;
    }

    public static String getValue(String desc , String name){
        for(EnumConfig c : EnumConfig.values()){
            if (c.desc.equals(desc) && c.name.equals(name)){
                return c.value;
            }
        }
        return null;
    }

}
