package com.zhengqing.modules.common.dto.input;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *  <p> 基础分页检索参数 </p>
 *
 * @description:
 * @author: zhengqing
 * @date: 2019/9/13 0013 1:57
 */
@ApiModel
@Data
public class BasePageQuery extends BaseInput {
    @ApiModelProperty(value = "当前页", required = true, position = 0)
    private Integer page = 1;
    @ApiModelProperty(value = "每页显示数量", required = true, position = 1)
    private Integer limit = 20;

    public Page getMybatisPage(){
        return new Page<>(this.page,this.limit);
    }
}
