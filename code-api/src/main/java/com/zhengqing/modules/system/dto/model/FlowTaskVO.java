package com.zhengqing.modules.system.dto.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zhengqing.modules.common.entity.BaseEntity;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 流程对象
 */
@Data
public class FlowTaskVO extends BaseEntity {

    private String id;

    private String name;

    private String processInstanceId;

    private String taskDefinitionKey;

    private String executionId;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;


}
