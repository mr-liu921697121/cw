package com.zhengqing.modules.system.service.impl;

import java.util.List;

import com.zhengqing.modules.system.entity.User;
import com.zhengqing.modules.system.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhengqing.modules.system.dto.input.RoleQueryPara;
import com.zhengqing.modules.system.entity.Role;
import com.zhengqing.modules.system.mapper.RoleMapper;
import com.zhengqing.modules.system.service.IRoleService;

/**
 * <p>
 * 系统管理-角色表 服务实现类
 * </p>
 *
 * @author: zhengqing
 * @date: 2019-08-20
 */
@Service
@Transactional
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

    @Autowired
    RoleMapper roleMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public void listPage(IPage<Role> page, RoleQueryPara filter) {
        page.setRecords(roleMapper.selectRoles(page, filter));
    }

    @Override
    public List<Role> list(RoleQueryPara filter) {
        return roleMapper.selectRoles(filter);
    }

    @Override
    public Integer saveOrUpdateData(Role para) {
        if (para.getId() != null) {
            roleMapper.updateById(para);
        } else {
            roleMapper.insert(para);
        }
        return para.getId();
    }

    public List<User> findUsersByRole(Integer role){
        List<User> list = userMapper.selectUserByRoleId(role);
        return list;
    }

}
