package com.zhengqing.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhengqing.modules.system.entity.SysConfig;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SysConfigMapper extends BaseMapper<SysConfig> {

    @Select("<script>" +
            "select * from t_sys_config where 1=1 <if test='type != null and type != &quot;&quot;'> and param_key=#{type} </if> order by sort asc" +
            "</script>")
    public List<SysConfig> findByType(@Param("type") String type);

}
