package com.zhengqing.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhengqing.modules.system.entity.SysConfig;

import java.util.List;

public interface ISysConfigService extends IService<SysConfig> {


    public List<SysConfig> findByType(String type);

}
