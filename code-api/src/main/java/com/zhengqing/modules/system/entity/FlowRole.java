package com.zhengqing.modules.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhengqing.modules.common.entity.BaseEntity;
import com.zhengqing.modules.common.validator.FieldRepeatValidator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * 系统管理-权限菜单表
 * </p>
 *
 * @author: zhengqing
 * @date: 2019-08-19
 */
@Data
@ApiModel(description = "流程审批节点设置角色")
@TableName("t_flow_role")
public class FlowRole {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    @ApiModelProperty(value = "流程名称")
    @TableField("flow_name")
    private String flowName;

    @ApiModelProperty(value = "流程节点")
    @TableField("flow_node")
    private String flowNode;

    @ApiModelProperty(value = "审批角色")
    @TableField("approval_role")
    private String approvalRole;


    @ApiModelProperty(value = "流程中candidateUsers值")
    @TableField("candidate_users")
    private String candidateUsers;

    @ApiModelProperty(value = "流程下标")
    @TableField("flow_index")
    private String flowIndex;

}
