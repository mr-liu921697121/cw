package com.zhengqing.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhengqing.modules.system.entity.SysConfig;
import com.zhengqing.modules.system.mapper.SysConfigMapper;
import com.zhengqing.modules.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {

    @Autowired
    private SysConfigMapper sysConfigMapper;

    public List<SysConfig> findByType(String type){
        List<SysConfig> list = sysConfigMapper.findByType(type);
        return list;
    }



}
