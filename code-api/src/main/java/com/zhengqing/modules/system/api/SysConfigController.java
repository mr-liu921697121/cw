package com.zhengqing.modules.system.api;

import com.zhengqing.modules.common.api.BaseController;
import com.zhengqing.modules.common.dto.output.ApiResult;
import com.zhengqing.modules.system.entity.SysConfig;
import com.zhengqing.modules.system.service.ISysConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/api/system/config")
@Api(description = "系统配置表 接口")
public class SysConfigController extends BaseController {

    @Autowired
    private ISysConfigService sysConfigService;

    @PostMapping(value = "/findByType")
    @ApiOperation(value = "根据类型查询数据", httpMethod = "POST", response = ApiResult.class)
    public ApiResult findByType( String type){
        List<SysConfig> list = sysConfigService.findByType(type);
        return ApiResult.ok("成功",list);
    }

}
