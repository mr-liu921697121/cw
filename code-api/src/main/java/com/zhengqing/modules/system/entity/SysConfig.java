package com.zhengqing.modules.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(description = "系统配置表")
@TableName("t_sys_config")
public class SysConfig {

    private String id;

    private String paramKey;

    private String paramName;

    private String paramValue;

    private String del_flag;

    private Date gmtCreate;

    private Date gmtModified;
}
