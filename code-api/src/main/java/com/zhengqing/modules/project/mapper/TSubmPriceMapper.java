package com.zhengqing.modules.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhengqing.modules.project.vo.TSubmPriceVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TSubmPriceMapper extends BaseMapper<TSubmPriceVo> {

    public IPage<TSubmPriceVo> findPage(Page page , @Param("params") TSubmPriceVo params);

    public List<TSubmPriceVo> findList();

}
