package com.zhengqing.modules.project.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhengqing.config.security.dto.SecurityUser;
import com.zhengqing.modules.common.api.EnumConfig;
import com.zhengqing.modules.common.dto.output.ApiResult;
import com.zhengqing.modules.project.entity.TSubmPrice;
import com.zhengqing.modules.project.mapper.TSubmPriceMapper;
import com.zhengqing.modules.project.service.TSubmPriceService;
import com.zhengqing.modules.project.vo.TSubmPriceVo;
import com.zhengqing.modules.system.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class TSubmPriceServiceImpl extends ServiceImpl<TSubmPriceMapper, TSubmPriceVo> implements TSubmPriceService {

    @Autowired
    private TSubmPriceMapper submPriceMapper;

    public ApiResult saveOrUpdatePro(TSubmPriceVo entity){
        User user = SecurityUser.getUser();
        QueryWrapper<TSubmPriceVo> proWrapper = new QueryWrapper<>();
        proWrapper.eq("good_name" , entity.getGoodName());
        proWrapper.eq("del_flag" , EnumConfig.DEL_FLAG_NOT_DELETE.value);
        proWrapper.eq("model" , entity.getModel());
        if (entity.getId()==null || entity.getId().equals("")){
            TSubmPrice exist = submPriceMapper.selectOne(proWrapper);
            if (exist!=null){
                return ApiResult.fail("该辅材已存在");
            }
            entity.setCreateBy(user.getId()+"");
            entity.setCreateTime(new Date());
            entity.setUpdateBy(user.getId()+"");
            entity.setUpdateTime(new Date());
            entity.setDelFlag(EnumConfig.DEL_FLAG_NOT_DELETE.value);
            entity.setId(UUID.randomUUID().toString().replace("-",""));
            submPriceMapper.insert(entity);
        }else {
            proWrapper.ne("id", entity.getId());
            TSubmPrice exist = submPriceMapper.selectOne(proWrapper);
            if (exist!=null){
                return ApiResult.fail("该辅材已存在");
            }
            entity.setUpdateBy(user.getId()+"");
            entity.setUpdateTime(new Date());
            submPriceMapper.updateById(entity);
        }
        return ApiResult.ok();
    }

    public ApiResult findPage(Page page , TSubmPriceVo enttiy){
        IPage<TSubmPriceVo> resultPage = submPriceMapper.findPage(page , enttiy);
        return ApiResult.ok("操作成功" , resultPage);
    }

    public ApiResult deleteById(TSubmPriceVo entity){
        User user = SecurityUser.getUser();
        entity.setDelFlag(EnumConfig.DEL_FLAG_DELETE.value);
        entity.setUpdateBy(user.getId()+"");
        entity.setUpdateTime(new Date());
        submPriceMapper.updateById(entity);
        return ApiResult.ok();
    }

    public ApiResult findList(){
        List<TSubmPriceVo> list = submPriceMapper.findList();
        return ApiResult.ok("成功", list);
    }

}
