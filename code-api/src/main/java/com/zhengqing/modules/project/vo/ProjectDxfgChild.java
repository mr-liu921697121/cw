package com.zhengqing.modules.project.vo;

import com.zhengqing.modules.common.anno.ExcelField;
import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectDxfgChild implements Serializable {

    @ExcelField(attrName = "ProductName" , title = "产品名称" ,sort = 0)
   // @ExcelProperty(value = "产品名称" , index = 0)
    private String productName;

    @ExcelField(attrName = "Model" , title = "型号", sort = 1)
   // @ExcelProperty(value = "型号" , index = 1)
    private String model;

    @ExcelField(attrName = "Long1" , title = "长" , sort = 2)
  //  @ExcelProperty(value = "长" , index = 2)
    private String long1;

    @ExcelField(attrName = "Wide" , title = "宽" , sort = 3)
   // @ExcelProperty(value = "宽" , index = 3)
    private String wide;

    @ExcelField(attrName = "Length" , title = "长度" , sort = 4)
  //  @ExcelProperty(value = "长度" , index = 4)
    private String length;

    @ExcelField(attrName = "Unit" , title = "单位" ,sort = 5)
  //  @ExcelProperty(value = "单位" , index = 5)
    private String unit;

    @ExcelField(attrName = "Quantity" , title = "数量" , sort = 6)
 //   @ExcelProperty(value = "数量" , index = 6)
    private String quantity;

    @ExcelField(attrName = "Area" , title = "单个面积" , sort = 7)
 //   @ExcelProperty(value = "单个面积" , index = 7)
    private String area;

    @ExcelField(attrName = "TotalArea" , title = "总面积" , sort = 8)
 //   @ExcelProperty(value = "总面积" , index = 8)
    private String totalArea;

    @ExcelField(attrName = "UnitPrice" , title = "单价" , sort = 9)
  //  @ExcelProperty(value = "单价" , index = 9)
    private String unitPrice;

    @ExcelField(attrName = "TotalPrices" , title = "总价" , sort = 10)
  //  @ExcelProperty(value = "总价" , index = 10)
    private String totalPrices;

    @ExcelField(attrName = "Remark" , title = "备注" , sort = 11)
  //  @ExcelProperty(value = "备注" , index = 11)
    private String  remark;

}
