package com.zhengqing.modules.project.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zhengqing.modules.common.dto.output.ApiResult;
import com.zhengqing.modules.project.entity.TSubmPrice;
import com.zhengqing.modules.project.vo.TSubmPriceVo;

public interface TSubmPriceService  extends IService<TSubmPriceVo> {

    public ApiResult saveOrUpdatePro(TSubmPriceVo entity);

    public ApiResult findPage(Page page , TSubmPriceVo enttiy);

    public ApiResult deleteById(TSubmPriceVo entity);

    public ApiResult findList();

}
