package com.zhengqing.modules.project.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhengqing.modules.common.api.BaseController;
import com.zhengqing.modules.common.dto.output.ApiResult;
import com.zhengqing.modules.project.vo.ProjectReqParamVo;
import com.zhengqing.modules.project.vo.TSysProCommentVo;
import com.zhengqing.modules.project.vo.TSysProjectVo;
import com.zhengqing.modules.project.service.ITSysProjectService;
import com.zhengqing.modules.system.entity.Role;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liusonglin
 * @since 2023-06-17
 */
@RestController
@RequestMapping("/api/system/project")
public class TSysProjectController extends BaseController {

    @Autowired
    private ITSysProjectService projectService;

    @PostMapping(value = "/save")
    @ApiOperation(value = "保存项目信息", httpMethod = "POST", response = ApiResult.class)
    public ApiResult save(@RequestBody TSysProjectVo entity) {
        return projectService.saveOrUpdatePro(entity);
    }

    @PostMapping(value = "/start")
    @ApiOperation(value = "提交流程", httpMethod = "POST", response = ApiResult.class)
    public ApiResult start(@RequestBody TSysProjectVo entity) {
        return projectService.start(entity);
    }

    @PostMapping(value = "/audit")
    @ApiOperation(value = "审批流程", httpMethod = "POST", response = ApiResult.class)
    public ApiResult audit(@RequestBody TSysProjectVo entity) {
        return projectService.audit(entity);
    }

    @PostMapping(value = "/deletePro")
    @ApiOperation(value = "删除", httpMethod = "POST", response = ApiResult.class)
    public ApiResult deletePro(@RequestBody TSysProjectVo entity) {
        return projectService.deletePro(entity);
    }

    @GetMapping(value = "/findPage")
    @ApiOperation(value = "列表", httpMethod = "POST", response = ApiResult.class)
    public ApiResult findPage(ProjectReqParamVo entity) {
        IPage<TSysProjectVo> resultPage = projectService.findPage(entity);
        return ApiResult.ok("操作成功" , resultPage);
    }


    @PostMapping(value = "/auditPage")
    @ApiOperation(value = "审核列表", httpMethod = "POST", response = ApiResult.class)
    public ApiResult auditPage(ProjectReqParamVo params) {
        IPage<TSysProjectVo> resultPage = projectService.auditPage(params);
        return ApiResult.ok("操作成功" , resultPage);
    }

    /**
     * orderStatus必传  2.审批完成待加工，3.已出库
     * @param params
     * @return
     */
    @PostMapping(value = "/processOrStockPage")
    @ApiOperation(value = "加工订单及出库订单列表", httpMethod = "POST", response = ApiResult.class)
    public ApiResult processOrStockPage(ProjectReqParamVo params) {
        Page page = new Page<>(params.getPage(), params.getLimit());
        IPage<TSysProjectVo> resultPage = projectService.processOrStockPage(page , params);
        return ApiResult.ok("操作成功" , resultPage);
    }

    @PostMapping(value = "/orderPage")
    @ApiOperation(value = "订单列表", httpMethod = "POST", response = ApiResult.class)
    public ApiResult orderPage(@RequestBody ProjectReqParamVo params) {
        Page page = new Page<>(params.getPage(), params.getLimit());
        Map<String , Object> resultPage = projectService.orderPage(page , params);
        return ApiResult.ok("操作成功" , resultPage);
    }


    @PostMapping(value = "/findCommentByProId")
    @ApiOperation(value = "审批意见", httpMethod = "POST", response = ApiResult.class)
    public ApiResult findCommentByProId(String proId) {
        List<TSysProCommentVo> list = projectService.findComment(proId);
        return ApiResult.ok("操作成功",list);
    }

    // 此方法先不要了，审核通过即订单状态为加工，然后加工这直接出厂，不要加工完成这一步
    @PostMapping(value = "/processFinish")
    @ApiOperation(value = "订单加工完成", httpMethod = "POST", response = ApiResult.class)
    public ApiResult processFinish(TSysProjectVo entity) {
        return projectService.processFinish(entity);
    }

    @PostMapping(value = "/stockOut")
    @ApiOperation(value = "订单出库", httpMethod = "POST", response = ApiResult.class)
    public ApiResult stockOut(@RequestBody TSysProjectVo entity) {
        return projectService.stockOut(entity);
    }


    @PostMapping(value = "/print")
    @ApiOperation(value = "打印" , response = ApiResult.class)
    public ApiResult print(HttpServletRequest request , @RequestBody TSysProjectVo entity) {
        return projectService.print(entity);
    }

    @RequestMapping(value = "/exportProjectInfoById")
    @ApiOperation(value = "导出详情" , response = ApiResult.class)
    public void exportProjectInfoById(HttpServletResponse response , TSysProjectVo entity) {
        projectService.exportProjectInfoById(response , entity);
    }


    public static String addressResolution(String address){
        String regex="(?<province>[^省]+自治区|.*?省|.*?行政区|.*?市)(?<city>[^市]+自治州|.*?地区|.*?行政单位|.+盟|市辖区|.*?市|.*?县)(?<county>[^县]+县|.+区|.+市|.+旗|.+海域|.+岛)?(?<town>[^区]+区|.+镇)?(?<village>.*)";
        Matcher m= Pattern.compile(regex).matcher(address);
        String province=null,city=null,county=null,town=null,village=null;
        Map<String,String> row=null;
        while(m.find()){
            row=new LinkedHashMap<String,String>();
            province=m.group("province");
            row.put("province", province==null?"":province.trim());
            city=m.group("city");
            row.put("city", city==null?"":city.trim());
            county=m.group("county");
            row.put("county", county==null?"":county.trim());
            town=m.group("town");
            row.put("town", town==null?"":town.trim());
            village=m.group("village");
            row.put("village", village==null?"":village.trim());
        }
        String resultAddress = row.get("province")+","+row.get("city")+","+row.get("county")+","+row.get("town")+row.get("village");
        return resultAddress;
    }

    public static void main(String[] args){
        Long expiresIn = 2592000L;  // 过期时间，秒
        String aaa = "2023-07-13 09:12:08";
        Long refresh = LocalDateTime.parse(aaa, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).atZone(ZoneOffset.systemDefault()).toEpochSecond();
        long a = expiresIn+refresh;
        Long curr = LocalDateTime.now().atZone(ZoneOffset.systemDefault()).toEpochSecond();
        System.out.println("........................a："+a+"...................curr："+curr);
        if (a>=curr){
            System.out.println("...................aaaa");
        }

        long bbb = curr-refresh;
        if (bbb>expiresIn){
            System.out.println("..........................大于30天");
        }else {
            System.out.println("......................小于30天");
        }
        System.out.println(addressResolution("湖北省武汉市洪山区光谷金融港光谷汇金中心5D"));
        System.out.println(addressResolution("湖北省恩施土家族苗族自治州恩施市"));
        System.out.println(addressResolution("北京市市辖区朝阳区"));
        System.out.println(addressResolution("内蒙古自治区兴安盟科尔沁右翼前旗"));
        System.out.println(addressResolution("西藏自治区日喀则地区日喀则市"));
        System.out.println(addressResolution("广西壮族自治区防城港市上思县地方"));
        System.out.println(addressResolution("海南省省直辖县级行政单位中沙群岛的岛礁及其海域"));


        String bb = "15927403811";
        System.out.println(bb.substring(7,11));
    }

}
