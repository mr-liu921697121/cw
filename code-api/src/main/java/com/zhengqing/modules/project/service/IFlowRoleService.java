package com.zhengqing.modules.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhengqing.modules.system.entity.FlowRole;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统管理-菜单表 服务类
 * </p>
 *
 * @author: zhengqing
 * @date: 2019-08-19
 */
public interface IFlowRoleService extends IService<FlowRole> {

    public List<FlowRole> findFlowRoleByUser(String userId);

    public List<FlowRole> findFlowRoleByFlowName(String flowName);

    public FlowRole findFlowRoleByUserAndFlowName(Map<String , String> params);

    public FlowRole findFirstByFlowName(String flowName);

}
