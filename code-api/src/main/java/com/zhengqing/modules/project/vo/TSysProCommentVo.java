package com.zhengqing.modules.project.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.zhengqing.modules.project.entity.TSysProComment;
import lombok.Data;

@Data
public class TSysProCommentVo extends TSysProComment {

    @TableField(exist = false)
    private String userNick;   //

}
