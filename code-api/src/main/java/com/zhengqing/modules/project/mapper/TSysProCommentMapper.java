package com.zhengqing.modules.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhengqing.modules.project.vo.TSysProCommentVo;
import com.zhengqing.modules.project.entity.TSysProComment;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liusonglin
 * @since 2023-06-17
 */
@Repository
public interface TSysProCommentMapper extends BaseMapper<TSysProComment> {


    @Select("select t1.*,t2.nick_name userNick from t_sys_pro_comment t1 left join t_sys_user t2 on t1.user_id=t2.id where t1.del_flag=0 and t1.project_id=#{proId} order by t1.create_time desc, flow_status asc")
    public List<TSysProCommentVo> findCommentByProId(@Param("proId") String proId);

}
