package com.zhengqing.modules.project.controller;


import com.zhengqing.modules.common.api.BaseController;
import com.zhengqing.modules.common.dto.output.ApiResult;
import com.zhengqing.modules.project.entity.TSubmPrice;
import com.zhengqing.modules.project.service.TSubmPriceService;
import com.zhengqing.modules.project.vo.TSubmPriceVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/system/submPrice")
public class TSubmPriceController extends BaseController {

    @Autowired
    private TSubmPriceService tSubmPriceService;

    @PostMapping(value = "/save")
    @ApiOperation(value = "保存项目信息", httpMethod = "POST", response = ApiResult.class)
    public ApiResult save(@RequestBody TSubmPriceVo entity) {
        tSubmPriceService.saveOrUpdatePro(entity);
        return ApiResult.ok("操作成功");
    }

    @RequestMapping(value = "/findPage")
    @ApiOperation(value = "查询列表", httpMethod = "POST", response = ApiResult.class)
    public ApiResult findPage(HttpServletRequest request , TSubmPriceVo entity) {
        return tSubmPriceService.findPage(this.getPage(request) , entity);
    }

    @RequestMapping(value = "/findList")
    @ApiOperation(value = "查询列表", httpMethod = "POST", response = ApiResult.class)
    public ApiResult findList(HttpServletRequest request) {
        return tSubmPriceService.findList();
    }

    @RequestMapping(value = "/delete")
    @ApiOperation(value = "删除", httpMethod = "POST", response = ApiResult.class)
    public ApiResult delete(HttpServletRequest request , TSubmPriceVo entity) {
        return tSubmPriceService.deleteById(entity);
    }

}
