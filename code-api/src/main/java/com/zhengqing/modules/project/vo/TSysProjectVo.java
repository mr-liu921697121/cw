package com.zhengqing.modules.project.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhengqing.modules.project.entity.TSysProject;
import lombok.Data;

import java.util.Map;

@Data
@TableName("t_sys_project")
public class TSysProjectVo extends TSysProject {

    // 制单人用这个
    @TableField(exist = false)
    private String nickName;

    @TableField(exist = false)
    private String flowContent;

    // 终审人姓名
    @TableField(exist = false)
    private String auditName;

    /**
     * 公司信息
     */
    @TableField(exist = false)
    private Map<String ,String>  companyInfo;

    /**
     * 打印机信息设置
     */
    @TableField(exist = false)
    private Map<String ,String>  printerSet;

}
