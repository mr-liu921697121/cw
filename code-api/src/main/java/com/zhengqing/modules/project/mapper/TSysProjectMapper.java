package com.zhengqing.modules.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhengqing.modules.project.vo.ProjectReqParamVo;
import com.zhengqing.modules.project.vo.TSysProjectVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liusonglin
 * @since 2023-06-17
 */
@Repository
public interface TSysProjectMapper extends BaseMapper<TSysProjectVo> {

    public IPage<TSysProjectVo> findPage(Page page , @Param("params") ProjectReqParamVo entity);

    public IPage<TSysProjectVo> auditPage(Page page , @Param("params") ProjectReqParamVo params);

    public IPage<TSysProjectVo> pageOrder(Page page , @Param("params") Map<String , Object> params);

    public IPage<TSysProjectVo> processOrStockPage(Page page , @Param("params") ProjectReqParamVo params);

    public TSysProjectVo printById(@Param("id") String id);

    public IPage<TSysProjectVo> orderPage(Page page , @Param("params") ProjectReqParamVo params);

    public Double countAmount(@Param("params") ProjectReqParamVo params);

}
