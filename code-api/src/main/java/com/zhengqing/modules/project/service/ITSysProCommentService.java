package com.zhengqing.modules.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhengqing.modules.project.entity.TSysProComment;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liusonglin
 * @since 2023-06-17
 */
public interface ITSysProCommentService extends IService<TSysProComment> {

}
