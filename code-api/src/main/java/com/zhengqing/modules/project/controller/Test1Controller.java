package com.zhengqing.modules.project.controller;

import com.zhengqing.modules.activiti.IFlowTaskService;
import com.zhengqing.modules.common.api.BaseController;
import com.zhengqing.modules.common.dto.output.ApiResult;
import io.swagger.annotations.Api;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.api.task.model.builders.TaskPayloadBuilder;
import org.activiti.api.task.runtime.TaskRuntime;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 流程1相关操作
 */
@RestController
@RequestMapping("/api/system/test1")
@Api(description = "流程1相关操作")
public class Test1Controller extends BaseController {

    @Autowired
    private IFlowTaskService flowTaskService;

    @Autowired
    private ProcessRuntime processRuntime;

    @Autowired
    private TaskRuntime taskRuntime;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @PostMapping(value = "/deploy", produces = "application/json;charset=utf-8")
    public ApiResult deploy(@RequestBody Map<String,String> params) {
        // 创建一个部署对象
        return flowTaskService.deploy(params);
    }

    /**
     * 发起流程 start
     * @param params
     * @return
     */
    @PostMapping(value = "/test2", produces = "application/json;charset=utf-8")
    public ApiResult test2(@RequestBody Map<String , String> params){
        return flowTaskService.start(params);
    }

    /**
     * 查询流程历史信息
     * @return
     */
    @PostMapping(value = "/findFlowHis", produces = "application/json;charset=utf-8")
    public ApiResult findFlowHis(@RequestBody Map<String , String> params){
        return flowTaskService.findFlowHis(params);
    }

    /**
     * 第二级审批节点查询代办任务
     * @return
     */
    @PostMapping(value = "/twoQUery", produces = "application/json;charset=utf-8")
    public ApiResult twoQUery(@RequestBody Map<String , String> params){
        //根据流程的key和任务负责人查询该负责人下的所有任务
        return flowTaskService.findCurrTask(params);
    }

    /**
     * 第二级审批节点查询代办任务
     * @return
     */
    @PostMapping(value = "/approvalFlow", produces = "application/json;charset=utf-8")
    public ApiResult approvalFlow(@RequestBody Map<String , String> params){
        //根据流程的key和任务负责人查询该负责人下的所有任务
        return flowTaskService.submit(params);
    }


    /**
     * 第二级审批节点查询代办任务
     * @return
     */
    @PostMapping(value = "/flowReturn", produces = "application/json;charset=utf-8")
    public ApiResult flowReturn(@RequestBody Map<String , String> params){
        return flowTaskService.flowReturn(params);
    }


    /**
     * 获取流程图
     * @param response
     * @return
     */
    @PostMapping(value = "/findFlowImage", produces = "application/json;charset=utf-8")
    public void findFlowImage(HttpServletResponse response,@RequestBody Map<String , String> params){
        flowTaskService.findFlowImage(params , response);
    }

    // -----------------------------------------------------------------------测试动态流程图
    @PostMapping(value = "/testdtStart", produces = "application/json;charset=utf-8")
    public ApiResult testdtStart(@RequestBody Map<String , String> params){
        Map<String , Object> map = new HashMap<>();
        map.put(params.get("candidateUsers"),params.get("user"));
        ProcessInstance instance = runtimeService.startProcessInstanceByKey(params.get("flowName"));
        String id = instance.getId();
        // 查询第一个任务
        TaskQuery taskQuery = taskService.createTaskQuery().active().processDefinitionKey(params.get("flowName"));
        // 设置流程实例id
        taskQuery.processInstanceId(id);
        taskQuery.taskDefinitionKey("task1");

        Task task = taskQuery.singleResult();

        taskService.claim(task.getId(), params.get("user"));
        List<String> list = new ArrayList<>();
        list.add("b");
       // taskRuntime.complete(TaskPayloadBuilder.complete().withTaskId(task.getId()).withVariable(params.get("nextCandidateUsers"), list).build());
        taskRuntime.complete(TaskPayloadBuilder.complete().withTaskId(task.getId()).withVariable(params.get("nextCandidateUsers"),"bbb").build());

        return ApiResult.ok();
    }

    /**
     * 第二级审批节点查询代办任务
     * @return
     */
    @PostMapping(value = "/testSubmit", produces = "application/json;charset=utf-8")
    public ApiResult testSubmit(@RequestBody Map<String , String> params){
        Map<String , Object> map = new HashMap<>();
        map.put("bbb","b1");

        // 查询第一个任务
        TaskQuery taskQuery = taskService.createTaskQuery().active().taskId(params.get("taskId")).processDefinitionKey(params.get("flowName"));
        // 设置流程实例id
        //  taskQuery.processInstanceId(id);

        Task task = taskQuery.singleResult();
        if (task!=null){
            boolean finishFlag = false;  // 是否最后一步
            task.setAssignee("dalao");
            taskService.claim(task.getId() , "dalao");
            taskRuntime.complete(TaskPayloadBuilder.complete().withTaskId(task.getId()).build());
//            if (!finishFlag){
//                List<User> userList = roleService.findUsersByRole(Integer.valueOf(nextRole.getApprovalRole()));
//                // taskService.complete(task.getId());
//                taskService.claim(task.getId(), user.getUsername());
//                taskRuntime.complete(TaskPayloadBuilder.complete()
//                        .withTaskId(task.getId()).withVariable(nextRole.getCandidateUsers().substring(2,nextRole.getCandidateUsers().length()-1), userList.stream().map(User::getUsername).collect(Collectors.toList())).build());
//            }else {
//                taskService.claim(task.getId(), user.getUsername());
//                taskRuntime.complete(TaskPayloadBuilder.complete().withTaskId(task.getId()).build());
//            }
        }
        // 发起流程之后还要保存业务代码，将流程
        // System.out.println("....................instanceId："+id);
        //instance.get
        return ApiResult.ok("操作成功");
    }

}
