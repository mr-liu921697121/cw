package com.zhengqing.modules.project.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhengqing.config.security.dto.SecurityUser;
import com.zhengqing.modules.activiti.IFlowTaskService;
import com.zhengqing.modules.common.api.EnumConfig;
import com.zhengqing.modules.common.dto.output.ApiResult;
import com.zhengqing.modules.common.enumeration.ResultCode;
import com.zhengqing.modules.common.util.ProjectExcelUtil;
import com.zhengqing.modules.project.vo.ProjectReqParamVo;
import com.zhengqing.modules.project.vo.TSysProCommentVo;
import com.zhengqing.modules.project.vo.TSysProjectVo;
import com.zhengqing.modules.system.entity.FlowRole;
import com.zhengqing.modules.project.entity.TSysProComment;
import com.zhengqing.modules.project.entity.TSysProject;
import com.zhengqing.modules.system.entity.SysConfig;
import com.zhengqing.modules.system.entity.User;
import com.zhengqing.modules.project.mapper.FlowRoleMapper;
import com.zhengqing.modules.project.mapper.TSysProCommentMapper;
import com.zhengqing.modules.project.mapper.TSysProjectMapper;
import com.zhengqing.modules.project.service.ITSysProjectService;
import com.zhengqing.modules.system.mapper.SysConfigMapper;
import com.zhengqing.modules.system.service.IUserService;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liusonglin
 * @since 2023-06-17
 */
@Service
public class TSysProjectServiceImpl extends ServiceImpl<TSysProjectMapper, TSysProjectVo> implements ITSysProjectService {

    @Autowired
    IUserService userService;

    @Autowired
    private IFlowTaskService flowTaskService;

    @Autowired
    private TSysProCommentMapper sysProCommentMapper;

    @Autowired
    private TSysProjectMapper tSysProjectMapper;

    @Autowired
    private FlowRoleMapper flowRoleMapper;

    @Autowired
    private SysConfigMapper sysConfigMapper;

    public ApiResult saveOrUpdatePro(TSysProjectVo entity){
        User user = SecurityUser.getUser();
        if (entity.getId()==null || entity.getId().equals("")){
            entity.setFlowStatus(EnumConfig.FLOW_STATUS_CREATE.value);
            entity.setOrderStatus(EnumConfig.PROJECT_ORDER_STATUS_0.value);
            entity.setCreateBy(user.getId()+"");
            entity.setCreateTime(new Date());
            entity.setUpdateBy(user.getId()+"");
            entity.setUpdateTime(new Date());
            entity.setDelFlag(EnumConfig.DEL_FLAG_NOT_DELETE.value);
            entity.setId(UUID.randomUUID().toString().replace("-",""));
            tSysProjectMapper.insert(entity);
        }else {
            entity.setUpdateBy(user.getId()+"");
            entity.setUpdateTime(new Date());
            tSysProjectMapper.updateById(entity);
        }
        return ApiResult.ok();
    }

    /**
     * 提交流程
     * @param entity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ApiResult start(TSysProjectVo entity){
        Map<String , String> params = new HashMap<>();
        if(entity.getProjectType().equals(EnumConfig.PROJECT_TYPE_DXFG.getValue())){
            params.put("flowName" , EnumConfig.FLOW_NAME_PROJECT_TYPE_DXFG.value);
        }
        if(entity.getProjectType().equals(EnumConfig.PROJECT_TYPE_YFL.getValue())){
            params.put("flowName" , EnumConfig.FLOW_NAME_PROJECT_TYPE_YFL.value);
        }
        ApiResult startResult = flowTaskService.start(params);
        if (startResult.getCode()== ResultCode.SUCCESS.getCode()){
            Map<String , Object> resultMap = (Map<String, Object>)startResult.getData();
            Task task = (Task)resultMap.get("task");
            entity.setExeId(task.getExecutionId());
            entity.setInstId(task.getProcessInstanceId());
            entity.setFlowStatus(EnumConfig.FLOW_STATUS_AUDIT.value);   // 状态由前端传吧
            this.saveOrUpdatePro(entity);

            //创建保存 审批记录
            this.saveFlowComment((String) resultMap.get("currRole"),(String) resultMap.get("nextRole") , entity.getId() , EnumConfig.FLOW_COMMENT_STATUS_1.value , "提交流程");
            return ApiResult.ok();
        }else {
            return startResult;
        }
    }

    public void saveFlowComment(String currRoleCode , String nextRoleCode , String projectId , String flowStatus , String msg){
        //创建保存 审批记录
        // 先查有没有上一条，有的话修改上一条，没有的话插入
        User user = SecurityUser.getUser();
        QueryWrapper<TSysProComment> commentWrapper = new QueryWrapper<>();
        commentWrapper.eq("project_id" , projectId);
        commentWrapper.eq("flow_status" , EnumConfig.FLOW_COMMENT_STATUS_0.value);
        TSysProComment queryComment = sysProCommentMapper.selectOne(commentWrapper);
        if (queryComment!=null){
            queryComment.setFlowStatus(flowStatus);
            queryComment.setFlowContent(msg);
            queryComment.setUserId(user.getId()+"");
            queryComment.setRoleCode(currRoleCode);
            queryComment.setCreateTime(new Date());
            sysProCommentMapper.updateById(queryComment);
        }else {
            TSysProComment comment = new TSysProComment();
            comment.setId(UUID.randomUUID().toString().replace("-",""));
            comment.setProjectId(projectId);
            comment.setFlowStatus(flowStatus);
            comment.setFlowContent(msg);
            comment.setDelFlag(EnumConfig.DEL_FLAG_NOT_DELETE.value);
            comment.setCreateTime(new Date());
            comment.setRoleCode(currRoleCode);
            comment.setUserId(user.getId()+"");
            sysProCommentMapper.insert(comment);
        }

        if (nextRoleCode!=null){
            TSysProComment nextComment = new TSysProComment();
            nextComment.setId(UUID.randomUUID().toString().replace("-",""));
            nextComment.setProjectId(projectId);
            nextComment.setFlowStatus(EnumConfig.FLOW_COMMENT_STATUS_0.value);
            nextComment.setFlowContent(null);
            nextComment.setDelFlag(EnumConfig.DEL_FLAG_NOT_DELETE.value);
            nextComment.setCreateTime(new Date());
            nextComment.setRoleCode(nextRoleCode);
            sysProCommentMapper.insert(nextComment);
        }


    }

    /**
     * 审批流程流程
     * @param entity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ApiResult audit(TSysProjectVo entity){
        User user = SecurityUser.getUser();

        //查询项目信息
        TSysProjectVo tSysProjectVo = this.getById(entity.getId());
        if(ObjectUtil.isEmpty(tSysProjectVo)){
            return ApiResult.fail(500 ,"未找到项目信息！");
        }
        if(tSysProjectVo.getInstId() == null){
            return ApiResult.fail(500 ,"项目流程尚未提交审核！");
        }

        Map<String , String> params = new HashMap<>();
        if(tSysProjectVo.getProjectType().equals(EnumConfig.PROJECT_TYPE_DXFG.value)){
            params.put("flowName" , EnumConfig.FLOW_NAME_PROJECT_TYPE_DXFG.value);
        }
        if(tSysProjectVo.getProjectType().equals(EnumConfig.PROJECT_TYPE_YFL.value)){
            params.put("flowName" , EnumConfig.FLOW_NAME_PROJECT_TYPE_YFL.value);
        }
        params.put("instId" , tSysProjectVo.getInstId());
        ApiResult auditResult = null;
        String nextRoleCode = null;
        String currRoleCode = null;
        if (entity.getFlowStatus().equals(EnumConfig.FLOW_COMMENT_STATUS_1.value)){
            // 同意
            auditResult = flowTaskService.submit(params);
        }else if(entity.getFlowStatus().equals(EnumConfig.FLOW_COMMENT_STATUS_2.value)) {
            // 退回
            params.put("index" , "1");
            auditResult = flowTaskService.flowReturn(params);
        }

        if (auditResult.getCode()==ResultCode.SUCCESS.getCode()){
            Map<String , Object> resultMap = (Map<String, Object>)auditResult.getData();
            String commentStatus = entity.getFlowStatus();
            if (entity.getFlowStatus().equals(EnumConfig.FLOW_COMMENT_STATUS_1.value)){
                if (resultMap.get("nextRole")==null){
                    entity.setFlowStatus(EnumConfig.FLOW_STATUS_PASS.value);
                    entity.setOrderStatus(EnumConfig.PROJECT_ORDER_STATUS_1.value);
                }else {
                    entity.setFlowStatus(EnumConfig.FLOW_STATUS_AUDIT.value);
                }
                nextRoleCode = resultMap.get("nextRole")==null?null:(String) resultMap.get("nextRole");
                currRoleCode = (String) resultMap.get("currRole");
            }else if(entity.getFlowStatus().equals(EnumConfig.FLOW_COMMENT_STATUS_2.value)) {
                // 退回
                entity.setFlowStatus(EnumConfig.FLOW_STATUS_RETURN.value);
                FlowRole fr = flowRoleMapper.findFirstByFlowName(params.get("flowName").toString());
                if (fr==null){
                    return ApiResult.fail(500 ,"流程未绑定退回节点！");
                }
                nextRoleCode = fr.getCandidateUsers().substring(2,fr.getCandidateUsers().length()-1);
                QueryWrapper<TSysProComment> spcWrapper = new QueryWrapper<>();
                spcWrapper.eq("project_id" , entity.getId());
                spcWrapper.isNull("user_id");
                spcWrapper.eq("flow_status" , EnumConfig.FLOW_COMMENT_STATUS_0.value);
                TSysProComment co = sysProCommentMapper.selectOne(spcWrapper);
                if(co==null){
                    return ApiResult.fail(500 ,"流程当前节点异常");
                }
                currRoleCode = co.getRoleCode();
            }
            //修改审核通过时间 ，审核状态  审核状态由前端传值。
            entity.setAuditPassTime(new Date());
            this.saveOrUpdatePro(entity);

            this.saveFlowComment(currRoleCode,nextRoleCode , entity.getId() , commentStatus , entity.getFlowContent());
            return ApiResult.ok();
        }else {
            return auditResult;
        }
    }

    /**
     * 删除项目
     * @param entity
     * @return
     */
    public ApiResult deletePro(TSysProjectVo entity){
        User user = SecurityUser.getUser();
        TSysProjectVo vo = tSysProjectMapper.selectById(entity.getId());
        if (vo==null){
            return ApiResult.fail("未查询到删除项目");
        }
        if (vo.getFlowStatus().equals(EnumConfig.FLOW_STATUS_AUDIT.value) || vo.getFlowStatus().equals(EnumConfig.FLOW_STATUS_PASS.value)){
            return ApiResult.fail("流程已提交，不能删除");
        }
        entity.setDelFlag(EnumConfig.DEL_FLAG_DELETE.value);
        entity.setUpdateTime(new Date());
        entity.setUpdateBy(user.getId()+"");
        tSysProjectMapper.updateById(entity);
        return ApiResult.ok();
    }


    public IPage<TSysProjectVo> findPage(ProjectReqParamVo entity){
        User user = SecurityUser.getUser();
        entity.setCreateBy(user.getId().toString());
        if(entity.getVueType()!=null){
            if(entity.getVueType().equals("1")){
                entity.setVueType("and flow_status in ('1','2' ,'3')");
            }
        }
        IPage<TSysProjectVo> resultPage = tSysProjectMapper.findPage(entity.getMybatisPage() , entity);
        return resultPage;
    }

    public IPage<TSysProjectVo> auditPage(ProjectReqParamVo params){
        User user = SecurityUser.getUser();
        List<FlowRole> frList = flowRoleMapper.findFlowRoleByUser(user.getId()+"");
        if (frList!=null && frList.size()>0){
            List<String> nodeList = frList.stream().map(FlowRole::getFlowNode).collect(Collectors.toList());
            params.setNodes(nodeList);
        }
        IPage<TSysProjectVo> resultPage = tSysProjectMapper.auditPage(params.getMybatisPage() , params);
        return resultPage;
    }

    public IPage<TSysProjectVo> processOrStockPage(Page page,ProjectReqParamVo params){
        User user = SecurityUser.getUser();
        List<FlowRole> frList = flowRoleMapper.findFlowRoleByUser(user.getId()+"");
        if (frList!=null && frList.size()>0){
            List<String> nodeList = frList.stream().map(FlowRole::getFlowNode).collect(Collectors.toList());
            params.setNodes(nodeList);
        }
        IPage<TSysProjectVo> resultPage = tSysProjectMapper.processOrStockPage(page , params);
        return resultPage;
    }

    public Map<String , Object> orderPage(Page page, ProjectReqParamVo params){
        IPage<TSysProjectVo> resultPage = tSysProjectMapper.orderPage(page , params);
        Map<String , Object> result = new HashMap<>();
        Double countAmount = tSysProjectMapper.countAmount(params);
        result.put("pageData" , resultPage);
        result.put("amount" , countAmount);
        return result;
    }

    /**
     * 根据项目id查询审批意见
     * @param proId
     * @return
     */
    public List<TSysProCommentVo> findComment(String proId){
        List<TSysProCommentVo> list = sysProCommentMapper.findCommentByProId(proId);
        for (TSysProCommentVo vo: list) {
            if (StringUtils.isEmpty(vo.getUserId())){
                List<User> userList = userService.selectUserByRoleCode(vo.getRoleCode());
                vo.setUserNick(userList.stream().map(User::getNickName).collect(Collectors.joining(",")));
            }
        }
        return list;
    }

    public ApiResult processFinish(TSysProjectVo entity){
        TSysProject pro = tSysProjectMapper.selectById(entity.getId());
        if (pro==null || !pro.getFlowStatus().equals(EnumConfig.FLOW_STATUS_PASS.value)){
            return ApiResult.fail("订单未审核完成，不能加工");
        }
        entity.setAuditPassTime(new Date());
        entity.setOrderStatus(EnumConfig.PROJECT_ORDER_STATUS_2.value);
        tSysProjectMapper.updateById(entity);
        return ApiResult.ok();
    }

    public ApiResult stockOut(TSysProjectVo entity){
        TSysProject pro = tSysProjectMapper.selectById(entity.getId());
        if (pro==null || !pro.getFlowStatus().equals(EnumConfig.FLOW_STATUS_PASS.value)){
            return ApiResult.fail("订单未加工完成，不能出库");
        }
        entity.setStockOutTime(new Date());
        entity.setOrderStatus(EnumConfig.PROJECT_ORDER_STATUS_2.value);
        tSysProjectMapper.updateById(entity);
        return ApiResult.ok();
    }

    public ApiResult print(TSysProjectVo entity){
        TSysProjectVo vo = tSysProjectMapper.printById(entity.getId());
        UpdateWrapper<TSysProjectVo> wrapper = new UpdateWrapper<>();
        wrapper.setSql("print_count=IFNULL(print_count,0)+1");
        wrapper.eq("id" , entity.getId());
        tSysProjectMapper.update(null , wrapper);
        List<SysConfig> company_info = sysConfigMapper.findByType("company_info");
        if(ObjectUtil.isNotNull(company_info)){
            Map<String, String> map = company_info.stream().collect(Collectors.toMap(SysConfig::getParamName, SysConfig::getParamValue));
            vo.setCompanyInfo(map);
        }
        List<SysConfig> printer_set = sysConfigMapper.findByType("printer_set");
        if(ObjectUtil.isNotNull(printer_set)){
            Map<String, String> map = printer_set.stream().collect(Collectors.toMap(SysConfig::getParamName, SysConfig::getParamValue));
            vo.setPrinterSet(map);
        }
        return ApiResult.ok("返回数据成功" , vo);
    }

    public void exportProjectInfoById (HttpServletResponse response , TSysProjectVo entity){
        TSysProjectVo vo = tSysProjectMapper.printById(entity.getId());
        List<SysConfig> list = sysConfigMapper.findByType("company_info");
//        EasyExcel.re
        ProjectExcelUtil a = new ProjectExcelUtil();
        a.exportExcel(response , list , vo);
    }

}
