package com.zhengqing.modules.project.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author liusonglin
 * @since 2023-06-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TSysProject extends Model {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    private String id;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 客户名称
     */
    private String clientName;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 报价日期，年-月-日
     */
    private String makeDate;

    /**
     * 是否含税，0.不含，1.含
     */
    private String pluxDutyFlag;

    /**
     * 是否含运费表示，0.不含，1.含
     */
    private String freightFlag;

    /**
     * 内容json
     */
    private String contextJson;

    /**
     * 类型，1.镀锌风管，2.圆法兰
     */
    private String projectType;

    /**
     * 状态，0.新建，1.已提交待审核，2.驳回，3.审核通过
     */
    private String flowStatus;

    /**
     * 订单状态，0.审批未完成，1.加工中，2.加工完成待出库，3.已出库
     */
    private String orderStatus;

    /**
     * 流程那边的id
     */
    private String exeId;

    private String instId;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    private String updateBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    private String delFlag;

    /**
     * 审核通过时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date auditPassTime;

    /**
     * 出库时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date stockOutTime;

    /**
     * 项目金额
     */
    private Double amount;

    /**
     * 打印次数
     */
    private Integer printCount;

}
