package com.zhengqing.modules.project.controller;

import com.zhengqing.modules.common.api.BaseController;
import com.zhengqing.modules.common.dto.output.ApiResult;
import com.zhengqing.modules.system.entity.FlowRole;
import com.zhengqing.modules.project.service.IFlowRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.activiti.api.process.model.ProcessDefinition;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.api.task.runtime.TaskRuntime;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.Process;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统管理-菜单表 接口
 * </p>
 *
 * @author: zhengqing
 * @description:
 * @date: 2019-08-19
 *
 */
@RestController
@RequestMapping("/api/system/flowRole")
@Api(description = "设置流程绑定审批角色")
public class SysFlowRoleController extends BaseController {

    @Autowired
    private ProcessRuntime processRuntime;

    @Autowired
    private TaskRuntime taskRuntime;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    IFlowRoleService flowRoleService;

    /**
     *
     * @param request
     * @param response
     * @param flowName   test1_start
     * @return
     */
    @PostMapping(value = "/findFlowNode")
    @ApiOperation(value = "获取流程节点", httpMethod = "POST", response = ApiResult.class)
    public ApiResult findFlowNode(HttpServletRequest request , HttpServletResponse response , @RequestBody Map<String , String> params) {
        //流程定义id
        // Page<ProcessDefinition> page = processRuntime.processDefinitions(Pageable.of(0, 100));
        ProcessDefinition definition = processRuntime.processDefinition(params.get("flowName"));
        BpmnModel bpmnModel = repositoryService.getBpmnModel(definition.getId());
        Process process = bpmnModel.getProcesses().get(0);
        //获取所有节点
        Collection<FlowElement> flowElements = process.getFlowElements();
        List<UserTask> UserTaskList = process.findFlowElementsOfType(UserTask.class);   // 只获取任务节点
        return ApiResult.ok("获取流程节点成功", UserTaskList);
    }

    @PostMapping(value = "/flowRoleSave")
    @ApiOperation(value = "保存流程绑定审批角色", httpMethod = "POST", response = ApiResult.class)
    public ApiResult flowRoleSave(HttpServletRequest request , HttpServletResponse response , FlowRole entity) {
        flowRoleService.save(entity);
        return ApiResult.ok("添加成功", null);
    }



}
