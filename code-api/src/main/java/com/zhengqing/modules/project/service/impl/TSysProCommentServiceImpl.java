package com.zhengqing.modules.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhengqing.modules.project.entity.TSysProComment;
import com.zhengqing.modules.project.mapper.TSysProCommentMapper;
import com.zhengqing.modules.project.service.ITSysProCommentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liusonglin
 * @since 2023-06-17
 */
@Service
public class TSysProCommentServiceImpl extends ServiceImpl<TSysProCommentMapper, TSysProComment> implements ITSysProCommentService {

}
