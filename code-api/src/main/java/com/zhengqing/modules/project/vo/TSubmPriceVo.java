package com.zhengqing.modules.project.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhengqing.modules.project.entity.TSubmPrice;
import lombok.Data;

@Data
@TableName("t_subm_price")
public class TSubmPriceVo extends TSubmPrice {

    @TableField(exist = false)
    private String nickName;

    @TableField(exist = false)
    private String priceStart;

    @TableField(exist = false)
    private String priceEnd;

}
