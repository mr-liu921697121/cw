package com.zhengqing.modules.project.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zhengqing.modules.common.dto.output.ApiResult;
import com.zhengqing.modules.project.vo.ProjectReqParamVo;
import com.zhengqing.modules.project.vo.TSysProCommentVo;
import com.zhengqing.modules.project.vo.TSysProjectVo;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liusonglin
 * @since 2023-06-17
 */
public interface ITSysProjectService extends IService<TSysProjectVo> {

    public ApiResult saveOrUpdatePro(TSysProjectVo entity);

    public ApiResult start(TSysProjectVo entity);

    public ApiResult audit(TSysProjectVo entity);

    public ApiResult deletePro(TSysProjectVo entity);

    public IPage<TSysProjectVo> findPage(ProjectReqParamVo entity);

    public IPage<TSysProjectVo> auditPage(ProjectReqParamVo params);

    public IPage<TSysProjectVo> processOrStockPage(Page page, ProjectReqParamVo params);


    public List<TSysProCommentVo> findComment(String proId);

    public ApiResult processFinish(TSysProjectVo entity);

    public ApiResult stockOut(TSysProjectVo entity);

    public ApiResult print(TSysProjectVo entity);

    public void exportProjectInfoById (HttpServletResponse response , TSysProjectVo entity);

    public Map<String , Object> orderPage(Page page, ProjectReqParamVo params);

}
