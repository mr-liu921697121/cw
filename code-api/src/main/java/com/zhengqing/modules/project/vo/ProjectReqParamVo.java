package com.zhengqing.modules.project.vo;

import com.zhengqing.modules.common.dto.input.BasePageQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectReqParamVo extends BasePageQuery {

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 客户电话
     */
    private String phone;

    /**
     * 客户名称
     */
    private String clientName;

    /**
     * 项目状态 状态，0.新建，1.已提交待审核，2.驳回，3.审核通过
     */
    private String flowStatus;

    private List<String> nodes;;

    /**
     * 类型，1.镀锌风管，2.圆法兰
     */
    private String projectType;

    /**
     * 前端页面类型； 1 审核订单 2 加工订单 3 出库订单 4 订单管理
     */
    private String vueType;

    private String orderStatus;

    /**
     * 报价日期
     */
    private String createTimeStart;

    /**
     * 报价日期
     */
    private String createTimeEnd;

    private String auditPassTime;

    private String createTime;
}
