package com.zhengqing.modules.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhengqing.modules.system.entity.FlowRole;
import com.zhengqing.modules.project.mapper.FlowRoleMapper;
import com.zhengqing.modules.project.service.IFlowRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统管理-菜单表 服务实现类
 * </p>
 *
 * @author: zhengqing
 * @date: 2019-08-19
 */
@Service
@Transactional
public class FlowRoleServiceImpl extends ServiceImpl<FlowRoleMapper, FlowRole> implements IFlowRoleService {

    @Autowired
    FlowRoleMapper flowRoleMapper;

    public List<FlowRole> findFlowRoleByUser(String userId){
        List<FlowRole> list = flowRoleMapper.findFlowRoleByUser(userId);
        return list;
    }

    public List<FlowRole> findFlowRoleByFlowName(String flowName){
        List<FlowRole> list = flowRoleMapper.findFlowRoleByFlowName(flowName);
        return list;
    }

    public FlowRole findFlowRoleByUserAndFlowName(Map<String , String> params){
        FlowRole flowRole = flowRoleMapper.findFlowRoleByUserAndFlowName(params);
        return flowRole;
    }

    public FlowRole findFirstByFlowName(String flowName){
        FlowRole fr = flowRoleMapper.findFirstByFlowName(flowName);
        return fr;
    }

}
