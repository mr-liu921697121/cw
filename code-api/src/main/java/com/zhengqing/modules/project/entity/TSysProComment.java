package com.zhengqing.modules.project.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author liusonglin
 * @since 2023-06-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TSysProComment extends Model {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    private String id;

    /**
     * 项目id
     */
    private String projectId;

    /**
     * 审批状态，0.未通过，1.已通过
     */
    private String flowStatus;

    /**
     * 审批意见
     */
    private String flowContent;

    /**
     * 角色编码
     */
    private String roleCode;

    /**
     * 审批人id
     */
    private String userId;

    private String delFlag;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;


}
