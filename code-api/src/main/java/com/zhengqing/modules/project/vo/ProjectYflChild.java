package com.zhengqing.modules.project.vo;

import com.zhengqing.modules.common.anno.ExcelField;
import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectYflChild implements Serializable {

    @ExcelField(attrName = "productName" , title = "产品名称" ,sort = 0)
    private String productName;

    @ExcelField(attrName = "Model" , title = "型号", sort = 1)
    private String model;

    @ExcelField(attrName = "Quantity" , title = "数量" , sort = 2)
    private String quantity;

    @ExcelField(attrName = "HoleCount" , title = "孔数量" , sort = 3)
    private String holeCount;

    @ExcelField(attrName = "UnitPrice" , title = "单价" , sort = 4)
    private String unitPrice;

    @ExcelField(attrName = "TotalPrices" , title = "总价" , sort = 5)
    private String totalPrices;

    @ExcelField(attrName = "Remark" , title = "备注" , sort = 6)
    private String remark;

    private String innerDiameter;   // 内直径

}
