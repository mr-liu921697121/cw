package com.zhengqing.modules.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhengqing.modules.system.dto.input.MenuQueryPara;
import com.zhengqing.modules.system.entity.FlowRole;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface FlowRoleMapper extends BaseMapper<FlowRole> {


//    @Select("<script>" +
//            "select t1.* from t_flow_role t1 INNER JOIN t_sys_role t2 on t1.approval_role=t2.id INNER JOIN t_sys_user_role t3 on t2.id=t3.role_id where t3.user_id=#{userId}" +
//            "</script>")
    public List<FlowRole> findFlowRoleByUser(@Param("userId") String userId);

//    @Select("<script>" +
//            "select t1.* from t_flow_role t1 INNER JOIN t_sys_role t2 on t1.approval_role=t2.id INNER JOIN t_sys_user_role t3 on t2.id=t3.role_id " +
//            "where t1.flow_name=#{flowName} order by t1.flow_index asc" +
//            "</script>")
    public List<FlowRole> findFlowRoleByFlowName(@Param("flowName") String flowName);

//    @Select("<script>" +
//            "select t1.* from t_flow_role t1 INNER JOIN t_sys_role t2 on t1.approval_role=t2.id INNER JOIN t_sys_user_role t3 on t2.id=t3.role_id " +
//            "where t3.user_id=#{params.userId} " +
//            "<if test='params.flowName != null and params.flowName != &quot;&quot;'> and t1.flow_name=#{params.flowName} </if>" +
//            "limit 1" +
//            "</script>")
    public FlowRole findFlowRoleByUserAndFlowName(@Param("params")Map<String , String> params);

    public FlowRole findFirstByFlowName(@Param("flowName")String flowName);

}
