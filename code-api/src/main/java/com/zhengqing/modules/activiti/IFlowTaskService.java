package com.zhengqing.modules.activiti;

import com.zhengqing.modules.common.dto.output.ApiResult;
import com.zhengqing.modules.system.entity.FlowRole;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface IFlowTaskService {

    public ApiResult findFlowNode(Map<String , String> params);

    public ApiResult flowRoleSave(FlowRole entity);

    /**
     * 部署任务
     * @param params
     * @return
     */
    public ApiResult deploy(Map<String,String> params);

    public ApiResult deploy2(Map<String,String> params);

    /**
     * 发起流程 start
     * @param params
     * @return
     */
    public ApiResult start(Map<String , String> params);

    /**
     * 发起人重新提交流程
     * @param params
     * @return
     */
    public ApiResult submit(Map<String , String> params);

    /**
     * 查询流程历史信息
     * @return
     */
    public ApiResult findFlowHis(Map<String , String> params);

    /**
     * 第二级审批节点查询代办任务
     * @return
     */
    public ApiResult findCurrTask(Map<String , String> params);

    /**
     * 第二级审批节点查询代办任务
     * @return
     */
    public ApiResult flowReturn(Map<String , String> params);


    /**
     * 获取流程图
     * @return
     */
    public void findFlowImage(Map<String , String> params , HttpServletResponse response);

}
