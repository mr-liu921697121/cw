package com.zhengqing.modules.activiti;

import javax.servlet.http.HttpServletResponse;

/**
 * 流程图servic
 */
public interface ProcessImageService {


    /**
     * 根据流程实例Id获取流程图
     *
     * @param processInstanceId 流程实例id
     * @throws Exception exception
     */
    void getFlowImgByInstanceId(String processInstanceId, HttpServletResponse response);

}

