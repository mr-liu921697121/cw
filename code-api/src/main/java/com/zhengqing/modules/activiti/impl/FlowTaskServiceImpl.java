package com.zhengqing.modules.activiti.impl;

import com.zhengqing.config.security.dto.SecurityUser;
import com.zhengqing.modules.activiti.IFlowTaskService;
import com.zhengqing.modules.common.dto.output.ApiResult;
import com.zhengqing.modules.system.dto.model.FlowTaskVO;
import com.zhengqing.modules.system.entity.FlowRole;
import com.zhengqing.modules.system.entity.User;
import com.zhengqing.modules.project.service.IFlowRoleService;
import com.zhengqing.modules.system.service.IRoleService;
import com.zhengqing.modules.system.service.IUserService;
import org.activiti.api.process.model.ProcessDefinition;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.api.task.model.builders.TaskPayloadBuilder;
import org.activiti.api.task.runtime.TaskRuntime;
import org.activiti.bpmn.model.*;
import org.activiti.bpmn.model.Process;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class FlowTaskServiceImpl implements IFlowTaskService {

    @Autowired
    private ProcessEngine processEngine;

    @Autowired
    private ProcessRuntime processRuntime;

    @Autowired
    private TaskRuntime taskRuntime;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IRoleService roleService;


    @Autowired
    private IFlowRoleService flowRoleService;

//    @Autowired
//    private ProcessImageService processImageService;


    public ApiResult findFlowNode(Map<String , String> params) {
        //流程定义id
        List<org.activiti.engine.repository.ProcessDefinition> list = ((ProcessDefinitionQuery)this.repositoryService.createProcessDefinitionQuery().
                processDefinitionKey(params.get("flowName")).orderByProcessDefinitionVersion().desc()).list();
        org.activiti.engine.repository.ProcessDefinition definition;
        if (!list.isEmpty()) {
            definition = (org.activiti.engine.repository.ProcessDefinition)list.get(0);
        } else {
            definition = this.repositoryService.getProcessDefinition(params.get("flowName"));
        }

        //ProcessDefinition definition = processRuntime.processDefinition(params.get("flowName"));
        BpmnModel bpmnModel = repositoryService.getBpmnModel(definition.getId());
        Process process = bpmnModel.getProcesses().get(0);
        //获取所有节点
        Collection<FlowElement> flowElements = process.getFlowElements();
        List<UserTask> UserTaskList = process.findFlowElementsOfType(UserTask.class);   // 只获取任务节点
        return ApiResult.ok("获取流程节点成功", UserTaskList);
    }

    public ApiResult flowRoleSave(FlowRole entity) {
        flowRoleService.save(entity);
        return ApiResult.ok("添加成功", null);
    }

    /**
     * 部署任务
     * @param params
     * @return
     */
    public ApiResult deploy(Map<String,String> params) {
        // 创建一个部署对象
        repositoryService.createDeployment().name(params.get("flowName"))
                .addClasspathResource("processes/yfl_start.bpmn20.xml").deploy();
        return ApiResult.ok("流程部署成功");
    }

    public ApiResult deploy2(Map<String,String> params) {
        // 创建一个部署对象
        if (params.get("refreshFlag").equals("1")){
            repositoryService.createDeployment().name(params.get("flowName"))
                    .addClasspathResource(params.get("filePath")).deploy();
        }else {
            List<Deployment> list = repositoryService.createDeploymentQuery().deploymentName(params.get("flowName")).list();
            if (list==null || list.size()==0){
                repositoryService.createDeployment().name(params.get("flowName"))
                        .addClasspathResource(params.get("filePath")).deploy();
            }
        }

        return ApiResult.ok("流程部署成功");
    }

    /**
     * 发起流程 start
     * @param params
     * @return
     */
    public ApiResult start(Map<String , String> params){
        User user = SecurityUser.getUser();
        Map<String , Object> resultMap = new HashMap<>();
        // 查询提交节点
        params.put("userId" , String.valueOf(user.getId()));
        FlowRole flowRole = flowRoleService.findFlowRoleByUserAndFlowName(params);
        if (flowRole==null || flowRole.getCandidateUsers()==null || flowRole.getCandidateUsers().equals("")){
            return ApiResult.fail("该节点未绑定角色");
        }
        String currRoleCode = flowRole.getCandidateUsers().substring(2,flowRole.getCandidateUsers().length()-1);
        resultMap.put("currRole" , currRoleCode);
        Map<String , Object> map = new HashMap<>();
        map.put(currRoleCode,user.getUsername());
        Authentication.setAuthenticatedUserId(user.getUsername());
        ProcessInstance instance = runtimeService.startProcessInstanceByKey(params.get("flowName"), map);
        String id = instance.getId();
        // 查询第一个任务
        TaskQuery taskQuery = taskService.createTaskQuery().active().processDefinitionKey(params.get("flowName"));
        // 设置流程实例id
        taskQuery.processInstanceId(id);

        Task task = taskQuery.singleResult();
        if (task!=null){
            // 查询下级审批人
            List<FlowRole> list = flowRoleService.findFlowRoleByFlowName(params.get("flowName"));
            if (list==null || list.size()==1){
                return ApiResult.fail("未绑定下级审批人");
            }
            FlowRole nextRole = null;
            for (int i=0;i<list.size();i++) {
                if (flowRole.getId().equals(list.get(i).getId())){
                    nextRole = list.get((i+1));
                }
            }
            List<User> userList = roleService.findUsersByRole(Integer.valueOf(nextRole.getApprovalRole()));
            // taskService.complete(task.getId());
            taskService.claim(task.getId(), user.getUsername());
            String nextRoleCode = nextRole.getCandidateUsers().substring(2,nextRole.getCandidateUsers().length()-1);
            taskRuntime.complete(TaskPayloadBuilder.complete()
                    .withTaskId(task.getId()).withVariable(nextRoleCode, userList.stream().map(User::getUsername).collect(Collectors.toList())).build());
            resultMap.put("nextRole" , nextRoleCode);
            resultMap.put("task" , task);
        }
        return ApiResult.ok("流程成功发起",resultMap);
    }

    /**
     * 发起人重新提交流程，这个跟上面的发起任务是一样的，只不过单拎出来是因为上面的发起流程还要包含保存表单内容的业务代码
     * 传taskId
     *   flowName
     * @param params
     * @return
     */
    public ApiResult submit(Map<String , String> params){
        User user = SecurityUser.getUser();
        Map<String , Object> resultMap = new HashMap<>();
        params.put("userId" , String.valueOf(user.getId()));
        FlowRole flowRole = flowRoleService.findFlowRoleByUserAndFlowName(params);
        if (flowRole==null || flowRole.getCandidateUsers()==null || flowRole.getCandidateUsers().equals("")){
            return ApiResult.fail("该节点未绑定角色");
        }
        Map<String , Object> map = new HashMap<>();
        String currRole = flowRole.getCandidateUsers().substring(2,flowRole.getCandidateUsers().length()-1);
        resultMap.put("currRole" , currRole);
        map.put(currRole,user.getUsername());
       // Authentication.setAuthenticatedUserId(user.getUsername());
        //ProcessInstance instance = runtimeService.startProcessInstanceByKey(params.get("flowName"), map);
       // String id = instance.getId();
        // 查询第一个任务
        TaskQuery taskQuery = taskService.createTaskQuery().active().processInstanceId(params.get("instId")).processDefinitionKey(params.get("flowName"));
        // 设置流程实例id
      //  taskQuery.processInstanceId(id);

        Task task = taskQuery.singleResult();
        if (task!=null){
            // 查询下级审批人
            List<FlowRole> list = flowRoleService.findFlowRoleByFlowName(params.get("flowName"));
            if (list==null || list.size()==1){
                return ApiResult.fail("未绑定下级审批人");
            }
            FlowRole nextRole = null;
            boolean finishFlag = false;  // 是否最后一步
            for (int i=0;i<list.size();i++) {
                if (flowRole.getId().equals(list.get(i).getId())){
                    // 还要判断一下是不是最后一级，如果是最后一级则不需要后面的步骤了。
                    if (i==list.size()-1){
                        finishFlag = true;
                    }else {
                        nextRole = list.get((i+1));
                    }
                }
            }
            if (!finishFlag){
                List<User> userList = roleService.findUsersByRole(Integer.valueOf(nextRole.getApprovalRole()));
                // taskService.complete(task.getId());
                String nextRoleCode = nextRole.getCandidateUsers().substring(2,nextRole.getCandidateUsers().length()-1);
                resultMap.put("nextRole" , nextRoleCode);
                taskService.claim(task.getId(), user.getUsername());
                taskRuntime.complete(TaskPayloadBuilder.complete()
                        .withTaskId(task.getId()).withVariable(nextRoleCode, userList.stream().map(User::getUsername).collect(Collectors.toList())).build());
            }else {
                taskService.claim(task.getId(), user.getUsername());
                taskRuntime.complete(TaskPayloadBuilder.complete().withTaskId(task.getId()).build());
            }
        }else {
            return ApiResult.fail("未查询到当前任务");
        }
        // 发起流程之后还要保存业务代码，将流程
       // System.out.println("....................instanceId："+id);
        //instance.get
        return ApiResult.ok("操作成功" , resultMap);
    }

    /**
     * 查询流程历史信息
     * @return
     */
    public ApiResult findFlowHis(Map<String , String> params){
        // 查询流程历史审批信息，只获取type=userTask的审批
        HistoricActivityInstanceQuery instanceQuery=historyService.createHistoricActivityInstanceQuery();
        instanceQuery.executionId(params.get("executionId")).activityType("userTask");
        instanceQuery.orderByHistoricActivityInstanceStartTime().desc();
        List<HistoricActivityInstance> list=instanceQuery.list();
        //输出查询结果
        for(HistoricActivityInstance hi:list){
            System.out.println(hi.getActivityId());
            System.out.println(hi.getActivityName());
            System.out.println(hi.getActivityType());
            System.out.println(hi.getAssignee());
            System.out.println(hi.getProcessDefinitionId());
            System.out.println(hi.getProcessInstanceId());
            System.out.println("------------------------");
        }

        // 转换成功后下面填业务代码，查询对应流程业务表中信息
        return ApiResult.ok("流程历史信息查询成功" ,list);
    }

    /**
     * 查询用户代办任务，只查询一种流程的代办任务，要传flowName
     * 第二级审批节点查询代办任务
     * @return
     */
    public ApiResult findCurrTask(Map<String , String> params){
        //根据流程的key和任务负责人查询该负责人下的所有任务
        User user = SecurityUser.getUser();
        params.put("userId" , String.valueOf(user.getId()));
        FlowRole flowRole = flowRoleService.findFlowRoleByUserAndFlowName(params);
        if (flowRole==null || flowRole.getCandidateUsers()==null || flowRole.getCandidateUsers().equals("")){
            return ApiResult.fail("该节点未绑定角色");
        }
        List<Task> list=taskService.createTaskQuery()
                .taskDefinitionKey(flowRole.getFlowNode())
                .listPage(Integer.valueOf(params.get("pageNo")) , Integer.valueOf(params.get("pageSize")));
        // 讲activiti的task对象转成系统FlowTaskVo
        List<FlowTaskVO> resultList = new ArrayList<>();
        for (Task t: list) {
            FlowTaskVO vo = new FlowTaskVO();
            vo.setId(t.getId());
            vo.setName(t.getName());
            vo.setCreateTime(t.getCreateTime());
            vo.setExecutionId(t.getExecutionId());
            vo.setTaskDefinitionKey(t.getTaskDefinitionKey());
            vo.setProcessInstanceId(t.getProcessInstanceId());
            resultList.add(vo);
        }
        // 转换成功后下面填业务代码，查询对应流程业务表中信息
        return ApiResult.ok("流程成功发起" ,resultList);
    }

    /**
     * 回退任务
     * taskId  任务id
     * index   回退到第几步
     * @return
     */
    public ApiResult flowReturn(Map<String , String> params){
        //org.activiti.engine.task.Task task = taskService.createTaskQuery().taskId(params.get("instId")).singleResult();
        TaskQuery taskQuery = taskService.createTaskQuery().active().processInstanceId(params.get("instId")).processDefinitionKey(params.get("flowName"));
        Task task = taskQuery.singleResult();
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        HistoryService historyService = processEngine.getHistoryService();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        TaskService taskService = processEngine.getTaskService();

        String processInstanceId = task.getProcessInstanceId();

        //  获取所有历史任务（按创建时间降序）
        List<HistoricTaskInstance> hisTaskList = historyService.createHistoricTaskInstanceQuery()
                .processInstanceId(processInstanceId)
                .orderByTaskCreateTime()
                .desc()
                .list();

        List<HistoricActivityInstance> hisActivityList = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(processInstanceId).list();

        if (CollectionUtils.isEmpty(hisTaskList) || hisTaskList.size() < 2) {
            return ApiResult.fail("流程驳回失败");
        }

        //  当前任务
        HistoricTaskInstance currentTask = hisTaskList.get(0);
        //  前一个任务
        HistoricTaskInstance lastTask = hisTaskList.get(hisTaskList.size()-Integer.valueOf(params.get("index")));
        //  当前活动
        HistoricActivityInstance currentActivity = hisActivityList.stream().filter(e -> currentTask.getId().equals(e.getTaskId())).collect(Collectors.toList()).get(0);
        //  前一个活动
        HistoricActivityInstance lastActivity = hisActivityList.stream().filter(e -> lastTask.getId().equals(e.getTaskId())).collect(Collectors.toList()).get(0);

        BpmnModel bpmnModel = repositoryService.getBpmnModel(task.getProcessDefinitionId());

        //  获取前一个活动节点
        FlowNode lastFlowNode = (FlowNode) bpmnModel.getMainProcess().getFlowElement(lastActivity.getActivityId());
        //  获取当前活动节点
        FlowNode currentFlowNode = (FlowNode) bpmnModel.getMainProcess().getFlowElement(currentActivity.getActivityId());

        //  临时保存当前活动的原始方向
        List<SequenceFlow> originalSequenceFlowList = new ArrayList<>();
        originalSequenceFlowList.addAll(currentFlowNode.getOutgoingFlows());
        //  清理活动方向
        currentFlowNode.getOutgoingFlows().clear();

        //  建立新方向
        SequenceFlow newSequenceFlow = new SequenceFlow();
        newSequenceFlow.setId("newSequenceFlowId");
        newSequenceFlow.setSourceFlowElement(currentFlowNode);
        newSequenceFlow.setTargetFlowElement(lastFlowNode);
        List<SequenceFlow> newSequenceFlowList = new ArrayList<>();
        newSequenceFlowList.add(newSequenceFlow);
        //  当前节点指向新的方向
        currentFlowNode.setOutgoingFlows(newSequenceFlowList);

        //  完成当前任务
        taskService.complete(task.getId());

        //  重新查询当前任务
        org.activiti.engine.task.Task nextTask = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult();
        if (null != nextTask) {
            taskService.setAssignee(nextTask.getId(), lastTask.getAssignee());
        }

        //  恢复原始方向
        currentFlowNode.setOutgoingFlows(originalSequenceFlowList);
        return ApiResult.ok("流程驳回成功");
    }


    /**
     * 获取流程图
     * @return
     */
    public void findFlowImage( Map<String , String> params , HttpServletResponse response){
       // processImageService.getFlowImgByInstanceId(params.get("processInstanceId") ,response);
    }

    /**
     * 查询用户待办任务列表。
     *
     * @param assignee 用户
     * @return 任务列表
     */
    public List<FlowTaskVO> queryToDoTasks(String assignee) {
        List<Task> taskList  = taskService.createTaskQuery().taskAssignee(assignee).list();

        List<FlowTaskVO> leaveTasks = new ArrayList<>();
        for (Task task : taskList) {
            FlowTaskVO vo = new FlowTaskVO();
            vo.setId(task.getId());
            vo.setName(task.getName());
            vo.setProcessInstanceId(task.getProcessDefinitionId());
            vo.setTaskDefinitionKey(task.getTaskDefinitionKey());
            vo.setExecutionId(task.getExecutionId());
            leaveTasks.add(vo);
        }
        return leaveTasks;
    }

    /**
     * 查询已处理任务列表。
     *
     * @param assignee 用户
     * @return 已处理任务列表
     */
    public List<FlowTaskVO> queryDoneTasks(String assignee) {
        List<HistoricTaskInstance> taskList  = historyService.createHistoricTaskInstanceQuery()
                .taskAssignee(assignee)
                .finished()
                .list();

        List<FlowTaskVO> leaveTasks = new ArrayList<>();
        for (HistoricTaskInstance task : taskList) {
            FlowTaskVO vo = new FlowTaskVO();
            vo.setId(task.getId());
            vo.setName(task.getName());
            vo.setProcessInstanceId(task.getProcessDefinitionId());
            vo.setTaskDefinitionKey(task.getTaskDefinitionKey());
            vo.setExecutionId(task.getExecutionId());
            leaveTasks.add(vo);
        }
        return leaveTasks;
    }

}
