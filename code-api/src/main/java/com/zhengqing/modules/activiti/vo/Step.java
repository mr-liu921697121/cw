package com.zhengqing.modules.activiti.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Step {
    private Integer id;
    private Integer workFlowId;
    private String name;
    private Integer type;
    private Integer orderId;
    //接下要执行的步骤，用于连线
    private List<Integer> chlidOrderIds;
    private String conditionExpression;
    private String assignee;

    private List<String> candidateUsers;

    private List<String> candidateGroups;
    //0为人工任务，1为服务于任务
    //private Integer taskType;

}