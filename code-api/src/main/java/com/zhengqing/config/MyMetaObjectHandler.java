package com.zhengqing.config;

import java.text.ParseException;
import java.util.Date;

import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.zhengqing.utils.DateTimeUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * MyBatisPlus自定义字段自动填充处理类 - 实体类中使用 @TableField注解
 * </p>
 *
 * @description: 注意前端传值时要为null
 * @author: zhengqing
 * @date: 2019/8/18 0018 1:46
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    /**
     * 创建
     */
    private final String GMT_CREATE = "gmtCreate";
    /**
     * 更新
     */
    private final String GMT_MODIFIED = "gmtModified";

    /**
     * 创建时间
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info(" -------------------- start insert fill ...  --------------------");
        if (metaObject.hasGetter(GMT_CREATE) && metaObject.hasGetter(GMT_MODIFIED)) {
            setFieldValByName(GMT_CREATE, new Date(), metaObject);
            setFieldValByName(GMT_MODIFIED, new Date(), metaObject);
        }

        // 日志输出 ================================================================================================
        Date gmtCreate = (Date)this.getFieldValByName(GMT_CREATE, metaObject);
        Date gmtModified = (Date)this.getFieldValByName(GMT_MODIFIED, metaObject);
        if (gmtCreate != null && gmtModified != null) {
            try {
                log.info("MyBatisPlus自动填充处理 - gmtCreate:{} gmtModified:{}",
                    DateTimeUtils.dateToDateTimeString(gmtCreate), DateTimeUtils.dateToDateTimeString(gmtModified));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * 最后一次更新时间
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info(" -------------------- start update fill ...  --------------------");
        if (metaObject.hasGetter(GMT_MODIFIED)) {
            setFieldValByName(GMT_MODIFIED, new Date(), metaObject);
        }

        // 日志输出 ================================================================================================
        Date gmtModified = (Date)this.getFieldValByName(GMT_MODIFIED, metaObject);
        if (gmtModified != null) {
            try {
                log.info("MyBatisPlus自动填充处理 - gmtModified:{}", DateTimeUtils.dateToDateTimeString(gmtModified));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
}
